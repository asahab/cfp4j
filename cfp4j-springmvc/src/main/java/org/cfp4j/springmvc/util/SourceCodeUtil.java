package org.cfp4j.springmvc.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import spoon.reflect.code.CtExpression;
import spoon.reflect.code.CtInvocation;
import spoon.reflect.declaration.CtElement;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtParameter;
import spoon.reflect.reference.CtTypeReference;

/**
 * Helper class that contains the common source code used in the cfp4j-springmvc module.
 *
 */
public class SourceCodeUtil {
	
	/** List of java.lang.Iterable class simple names*/
	public static final List<String> ITERABLE_TYPES = Collections.unmodifiableList(Arrays.asList("ITERABLE", "COLLECTION", "LIST", "SET"));	
	
	public static final List<String> PRIMITIVE_OBJECT_TYPES = Collections.unmodifiableList(Arrays.asList("Short", "Integer", "Float", "Double","String"));	
	
	/** Name Suffix for a data group collection */
	public static final String DATA_GROUP_ITERABLE_TYPE_NAME_SUFFIX = "_LIST";

    public static final Predicate<CtParameter<?>> PRIMITIVE_PARAMETER_PREDICATE = p -> p.getType().isPrimitive();
    public static final Predicate<CtParameter<?>> PRIMITIVE_OBJECT_PARAMETER_PREDICATE = p -> PRIMITIVE_OBJECT_TYPES.contains(p.getType().getSimpleName());
    public static final BiPredicate<String, CtParameter<?>> APPLICATION_ENTITY_PARAMETER_PREDICATE = (rootPackage, param) -> param.getType().getQualifiedName().startsWith(rootPackage);
    public static final BiPredicate<String, CtTypeReference<?>> APPLICATION_ENTITY_TYPEREFERENCE_PREDICATE = (rootPackage, typeRef) -> typeRef.getQualifiedName().startsWith(rootPackage);
    private static final Predicate<CtElement> INSTANCE_OF_CTINVOCATION_PREDICATE = e -> e instanceof CtInvocation;
    
    /**
     * Method that extract the CtInvocation instances from the body of the provided method.
     * 
     * @param method CtMethod instance
     * @return Set<CtInvocation<?>>
     */
    @SuppressWarnings("rawtypes")
	public static Set<CtInvocation> extracExecutableCallsFromMethodBody(CtMethod<?> method){
    	if(method.getBody() == null) {
    		return Collections.emptySet();
    	}
          return Optional.ofNullable(method.getBody().getElements(e -> e instanceof CtExpression))
                  .map(Collection::stream)
                  .orElseGet(Stream::empty)
                  .filter(INSTANCE_OF_CTINVOCATION_PREDICATE)
                  .map(e -> (CtInvocation)e)
                  .collect(Collectors.toSet());
    }
    
    /**
     * Add a private constructor to hide the implicit public one
     */
    private SourceCodeUtil() {}
}
