package org.cfp4j.springmvc;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.cfp4j.ICfpCalculator;
import org.cfp4j.entity.Reference;
import org.cfp4j.springmvc.analyzer.ControllerClassAnalyzer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spoon.reflect.CtModel;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtElement;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtType;
import spoon.reflect.reference.CtTypeReference;

/**
 * Class that implements the COSMIC Functional Points Calculator of a Spring-MVC application.
 *
 */
public class SpringMvcCfpCalculator implements ICfpCalculator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SpringMvcCfpCalculator.class);

	private final ControllerClassAnalyzer controllerClassAnalyzer;
	private final String appRootPackage;
	
	public SpringMvcCfpCalculator(String appRootPackage) {
		this.controllerClassAnalyzer = new ControllerClassAnalyzer(appRootPackage);
		this.appRootPackage = appRootPackage;
	}


	@Override
	public Set<Reference> doCalculation(CtModel model) {
		final Map<String, CtMethod<?>> appCtMethods = mapAllApplicationMethodFunction.apply(model, this.appRootPackage);
		// select all classes with Controller class annotation
		final List<CtClass<?>> controllers = model.getElements(CONTROLLER_TYPE_PREDICATE::test);
		LOGGER.info("Starting the CFP calculation: controller_count={}", controllers.size());
		return controllers.stream()
				.map(c -> this.controllerClassAnalyzer.analyze(c,appCtMethods))
				.filter(REFERENCE_FUNCTIONAL_POINTS_PREDICATE)
				.collect(Collectors.toSet());
	}


	/**
	 * Function used to build and return a map that contains all the application Ct_methods. the function parameters are:
	 * <ul>
	 * <li>model CtModel instance</li>
	 * <li>appRootPackage the root package of the current application to analyze</li>
	 * </ul>
	 * <p>
	 * The map key value format is "%s.%s" where the first occurrence is the class qualified name and the second on is the method signature.
	 * The map the value is the CtMethod instance.
	 * </p>
	 */
	private BiFunction<CtModel, String, Map<String, CtMethod<?>>> mapAllApplicationMethodFunction = (model, rootPackage) -> {
		final Map<String, CtMethod<?>> appCtMethods = new HashMap<>();
		final Map<String, CtType<?>> appTypeThatImplementedInterface = new HashMap<>();
    	model.getAllTypes().stream()
    	.filter(t -> t.getQualifiedName().startsWith(rootPackage))
    	.filter(t->t.isClass())
    	.filter (SERVICE_TYPE_PREDICATE)
    	.forEach(t->t.getSuperInterfaces().stream()
    			.filter(i->i.getQualifiedName().startsWith(rootPackage))
    			.findFirst().ifPresent(i-> appTypeThatImplementedInterface.put(i.getQualifiedName(), t)));
		
		
		BiConsumer<CtType<?>, Map<String, CtMethod<?>>> ctMethodMapConsumer = (t, map) -> t.getAllMethods().forEach(m->{
			if(t.isInterface() && appTypeThatImplementedInterface.containsKey(t.getQualifiedName())) {
				
				CtTypeReference<?>[] params = Optional.ofNullable(m.getParameters())
												.map(Collection::stream)
												.orElseGet(Stream::empty)
												.map(p -> p.getType())
												.toArray(CtTypeReference<?>[]::new);
				final CtMethod<?> implementedMethod = appTypeThatImplementedInterface.get(t.getQualifiedName()).getMethod(m.getType(), m.getSimpleName(), params);
				map.put(String.format("%s.%s",t.getQualifiedName(),m.getSignature()), implementedMethod);
			} else {
				map.put(String.format("%s.%s",t.getQualifiedName(),m.getSignature()), m);
			}
		});
		model.getAllTypes().stream().filter(t -> t.getQualifiedName().startsWith(rootPackage)).forEach(t-> ctMethodMapConsumer.accept(t, appCtMethods));
		return appCtMethods;
	};


	
    /** Predicate used to test if the provided CtElement instance is a spring controller class or not */
    private static final Predicate<CtElement> CONTROLLER_TYPE_PREDICATE = e -> e.getAnnotations().stream().anyMatch(a -> a.getType().getSimpleName().equals("Controller"));
    
    private static final Predicate<CtElement> SERVICE_TYPE_PREDICATE = e -> e.getAnnotations().stream().anyMatch(a -> a.getType().getSimpleName().equals("Service"));
    
    /** Predicate used to test if the provided reference contains a valid functional point count */
    private static final Predicate<Reference> REFERENCE_FUNCTIONAL_POINTS_PREDICATE = ref -> ref.functionalPoints() > 1;
}
