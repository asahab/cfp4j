package org.cfp4j.springmvc.extractor;

import static org.cfp4j.springmvc.util.SourceCodeUtil.PRIMITIVE_OBJECT_PARAMETER_PREDICATE;
import static org.cfp4j.springmvc.util.SourceCodeUtil.PRIMITIVE_PARAMETER_PREDICATE;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.cfp4j.entity.Movement;
import org.cfp4j.springmvc.model.DataMovement;
import org.cfp4j.springmvc.util.SourceCodeUtil;
import spoon.reflect.code.CtVariableRead;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtParameter;
import spoon.reflect.reference.CtTypeReference;

/**
 * A class representing the algorithm to extract the COSMIC Function Points related to the Entry
 * data movements. The targeted methods are the implemented methods in a controller class and
 * annotated with MappingRequest, MappingPost or MappingGet SpringMVC annotations.
 */
public class MappingMethodEntryMovementExtractor {

  private static final Predicate<CtParameter<?>> PATH_VARIABLE_PARAMETER_PREDICATE = p -> p
      .getAnnotations().stream().anyMatch(a -> a.getType().getSimpleName().equals("PathVariable"));
  private static final Predicate<CtParameter<?>> REQUEST_PARAMETER_PREDICATE = p -> p
      .getAnnotations().stream().anyMatch(a -> a.getType().getSimpleName().equals("RequestParam"));

  private final String appRootPackage;

  public MappingMethodEntryMovementExtractor(String appRootPackage) {
    this.appRootPackage = Objects.requireNonNull(appRootPackage);
  }


  /**
   * 
   * Function that extracts the CtParameter instances associated to the application domain entities.
   * An application domain entity type qualified name is started with the provided
   * {@code appRootPackage} that represents the main package of the application to analyze.
   * 
   * The function return is a set of CtTypeReference instances.
   * 
   */
  private static BiFunction<List<CtParameter<?>>, String, Set<CtTypeReference<?>>> extractAppDomainEntities =
      (params, rootPackage) -> Optional.ofNullable(params).map(Collection::stream)
          .orElseGet(Stream::empty)
          .filter(p -> SourceCodeUtil.APPLICATION_ENTITY_PARAMETER_PREDICATE.test(rootPackage, p))
          .map(CtParameter::getType).collect(Collectors.toSet());

  /**
   * 
   * Function that extracts the application domain entity setter invocation argument names from the
   * provided method body.
   * 
   * It aims to provide the entry parameter that will be excluded from the entry data groups as this
   * parameter is considered as an attribute of a data group that was already added as entry data
   * group.
   * 
   * The function return is a set of setter argument names.
   * 
   */
  private static BiFunction<CtMethod<?>, Set<CtTypeReference<?>>, Set<String>> extractAppDomainEntitySetterArgumentName =
      (method, appDomainEntityTypes) -> {
        final Set<String> appDomainEntityQualifiedNames = appDomainEntityTypes.stream()
            .map(CtTypeReference::getQualifiedName).collect(Collectors.toSet());
        return SourceCodeUtil.extracExecutableCallsFromMethodBody(method).stream().filter(i -> {
          // The declaration type can be null in case the source of the invocation is located in the
          // same package (no import statement),
          // or the executable is declared in the parent class that was inherited by the class
          // source of the invocation.
          CtTypeReference<?> typeRef = i.getExecutable().getDeclaringType();
          // To prevent the scenarios above, we look for the Reference type from the invocation
          // target instance before the execution declaring type
          CtTypeReference<?> targetTypeRef =
              (i.getTarget() != null) && (i.getTarget() instanceof CtVariableRead)
                  && ((CtVariableRead<?>) i.getTarget()).getVariable() != null
                      ? ((CtVariableRead<?>) i.getTarget()).getVariable().getType()
                      : null;
          final String typeRefQualifiedName = typeRef != null ? typeRef.getQualifiedName() : null;
          final String qualifiedName =
              targetTypeRef != null ? targetTypeRef.getQualifiedName() : typeRefQualifiedName;
          return appDomainEntityQualifiedNames.contains(qualifiedName);

        }).filter(i -> i.getExecutable().getSimpleName().startsWith("set"))
            .filter(i -> i.getExecutable().getParameters().size() == 1)
            .map(i -> i.getArguments().get(0).toString()).collect(Collectors.toSet());
      };

  /**
   * 
   * Function that extracts the CtParameter instances associated to the application domain entities
   * and convert it to entry data movements.
   * 
   * An application domain entity type qualified name is started with the provided
   * {@code appRootPackage} that represents the main package of the application to analyze.
   * 
   * This function is the implementation of the COSMIC Mapping Rule #01:
   * <p>
   * Any object of interest found among the input parameters of the method having one of the
   * annotations {@code @PostMapping}, {@code @GetMapping}, {@code @RequestMapping} or
   * {@code @ModelAttribute} is considered as an entry data movement
   * <p>
   * 
   * The function return is a set of DataMovement instances.
   * 
   */
  private static BiFunction<List<CtParameter<?>>, String, Set<DataMovement>> extractAndConvertAppDomainEntitiesToEntryDataMovements =
      (params, rootPackage) -> Optional.ofNullable(params).map(Collection::stream)
          .orElseGet(Stream::empty)
          .filter(p -> SourceCodeUtil.APPLICATION_ENTITY_PARAMETER_PREDICATE.test(rootPackage, p))
          .map(CtParameter::getType).map(CtTypeReference::getSimpleName)
          .map(ep -> new DataMovement(ep.toUpperCase(), Movement.ENTRY))
          .collect(Collectors.toSet());
  /**
   * Function that extracts the CtParameter instances associated to the primitive type that is
   * annotated with {@code @RequestParam}.
   * 
   * This function is the implementation of the COSMIC Mapping Rule #02:
   * <p>
   * All input parameters having the annotation {@code @RequestParam} of a method having one of the
   * annotations {@code @ModelAttribute}, {@code @PostMapping}, {@code @GetMapping} or
   * {@code @RequestMapping} are considered as a single entry data movement representing a filter of
   * search criteria. A search criterion is excluded from the filter if it represents an attribute
   * of an object of interest found among the input parameters of the method
   * 
   * The function return is an optional of DataMovement.
   * 
   */
  private static BiFunction<CtMethod<?>, String, Optional<DataMovement>> extractAndConvertPrimitiveRequestParametersToEntryDataMovement =
      (method, rootPackage) -> {
        // The method body can contain an application entity setter method that set a primitive
        // entry parameter to an application entity entry parameter.
        // in this case we do not consider the primitive parameter as an entry data movement.
        final Set<CtTypeReference<?>> appEntityParameterTypes =
            extractAppDomainEntities.apply(method.getParameters(), rootPackage);
        final Set<String> appEntitySetterArguments =
            extractAppDomainEntitySetterArgumentName.apply(method, appEntityParameterTypes);
        Set<String> requestParamNames = Optional.ofNullable(method.getParameters())
            .map(Collection::stream).orElseGet(Stream::empty)
            .filter(PRIMITIVE_PARAMETER_PREDICATE.or(PRIMITIVE_OBJECT_PARAMETER_PREDICATE))
            .filter(REQUEST_PARAMETER_PREDICATE)
            .filter(dm -> !appEntitySetterArguments.contains(dm.getSimpleName()))
            .map(CtParameter::getSimpleName).map(String::toUpperCase).collect(Collectors.toSet());

        return requestParamNames.isEmpty() ? Optional.empty()
            : Optional
                .of(new DataMovement("FILTER " + requestParamNames.toString(), Movement.ENTRY));
      };
  /**
   * Function that extracts the CtParameter instances associated to the primitive type that is
   * annotated with {@code @PathVariable}.
   * 
   * This function is the implementation of the COSMIC Mapping Rule #03:
   * <p>
   * All input parameters having the annotation {@code @PathVariable} of a method having one of the
   * annotations {@code @ModelAttribute}, {@code @PostMapping}, {@code @GetMapping} or
   * {@code @RequestMapping} are considered as a single entry data movement representing a filter of
   * search criteria. A search criterion is excluded from the filter if it represents an attribute
   * of an object of interest found among the input parameters of the method
   * 
   * The function returns a set of entry DataMovement instances.
   * 
   */
  private static BiFunction<CtMethod<?>, String, Set<DataMovement>> extractAndConvertPrimitivePathVariableParametersToEntryDataMovements =
      (method, rootPackage) -> {
        // The method body can contain an application entity setter method that set a primitive
        // entry parameter to an application entity entry parameter.
        // in this case we do not consider the primitive parameter as an entry data movement.
        final Set<CtTypeReference<?>> appEntityParameterTypes =
            extractAppDomainEntities.apply(method.getParameters(), rootPackage);
        final Set<String> appEntitySetterArguments =
            extractAppDomainEntitySetterArgumentName.apply(method, appEntityParameterTypes);
        return Optional.ofNullable(method.getParameters()).map(Collection::stream)
            .orElseGet(Stream::empty)
            .filter(PRIMITIVE_PARAMETER_PREDICATE.or(PRIMITIVE_OBJECT_PARAMETER_PREDICATE))
            .filter(PATH_VARIABLE_PARAMETER_PREDICATE)
            .filter(dm -> !appEntitySetterArguments.contains(dm.getSimpleName()))
            .map(CtParameter::getSimpleName)
            .map(pp -> new DataMovement(pp.toUpperCase(), Movement.ENTRY))
            .collect(Collectors.toSet());
      };

  /**
   * Function that extracts the CtParameter instances with the type {@code MultipartFile}
   * 
   * This function is the implementation of the COSMIC Mapping Rule #04:
   * <p>
   * Input parameters having the type {@code MultipartFile} of a method having one of the
   * annotations {@code @ModelAttribute}, {@code @PostMapping}, {@code @GetMapping} or
   * {@code @RequestMapping} are considered as a entry data movement that represents an attribute of
   * an object of interest.
   * 
   * The function returns a set of entry DataMovement instances.
   * 
   */
  private static Function<List<CtParameter<?>>, Set<DataMovement>> extractAndConvertMultipartFileParametersToEntryDataMovements =
      params -> Optional.ofNullable(params).map(Collection::stream).orElseGet(Stream::empty)
          .filter(p -> "MultipartFile".equals(p.getType().getSimpleName()))
          .map(p -> new DataMovement(p.getSimpleName().toUpperCase(), Movement.ENTRY))
          .collect(Collectors.toSet());

  /**
   * Returns a set containing all of the Entry data movements of the provided method.
   * 
   * @param method current method to analyze
   * @return an set containing all of the entry data movements in the provided method
   */

  public Set<DataMovement> extract(CtMethod<?> method) {
    // Convert entity entry params to data group entry movement
    final Set<DataMovement> entityParams = extractAndConvertAppDomainEntitiesToEntryDataMovements
        .apply(method.getParameters(), this.appRootPackage);

    // extract Primitive request entry params as data group entry movement
    final Optional<DataMovement> requestParameters =
        extractAndConvertPrimitiveRequestParametersToEntryDataMovement.apply(method,
            this.appRootPackage);
    // Primitive path variable entry params as data group entry movement
    final Set<DataMovement> pathVariableParameters =
        extractAndConvertPrimitivePathVariableParametersToEntryDataMovements.apply(method,
            this.appRootPackage);
    // extract multipartFile entry params as data group entry movement
    final Set<DataMovement> multipartFileParameters =
        extractAndConvertMultipartFileParametersToEntryDataMovements.apply(method.getParameters());
    // entries
    final Set<DataMovement> entries = Stream
        .concat(multipartFileParameters.stream(),
            Stream.concat(entityParams.stream(), pathVariableParameters.stream()))
        .collect(Collectors.toSet());
    if (requestParameters.isPresent()) {
      entries.add(requestParameters.get());
    }
    return entries;

  }

}
