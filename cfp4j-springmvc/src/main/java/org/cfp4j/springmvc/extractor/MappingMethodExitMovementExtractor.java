package org.cfp4j.springmvc.extractor;

import static org.cfp4j.springmvc.util.SourceCodeUtil.DATA_GROUP_ITERABLE_TYPE_NAME_SUFFIX;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.cfp4j.entity.Movement;
import org.cfp4j.springmvc.model.DataMovement;
import org.cfp4j.springmvc.util.SourceCodeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spoon.reflect.code.CtConstructorCall;
import spoon.reflect.code.CtExpression;
import spoon.reflect.code.CtFieldRead;
import spoon.reflect.code.CtInvocation;
import spoon.reflect.code.CtLocalVariable;
import spoon.reflect.code.CtReturn;
import spoon.reflect.code.CtStatement;
import spoon.reflect.code.CtVariableRead;
import spoon.reflect.declaration.CtElement;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtParameter;
import spoon.reflect.declaration.CtType;
import spoon.reflect.reference.CtExecutableReference;
import spoon.reflect.reference.CtTypeReference;

/**
 * A class representing the algorithm to extract the COSMIC Function Points related to the Exit data
 * movements. The targeted methods are the implemented methods in a controller class and annotated
 * with MappingRequest or MappingPost or MappingGet SpringMVC annotations.
 */
public class MappingMethodExitMovementExtractor {

  private static final Logger LOGGER =
      LoggerFactory.getLogger(MappingMethodExitMovementExtractor.class);

  /**
   * SpringMVC type (holder for model attributes and used as a mapping method entry parameter)
   * simple names
   */
  private static final List<String> MODEL_ATTRIBUTE_TYPE_SIMPLE_NAMES =
      Arrays.asList("Map", "Model", "ModelMap");
  /** SpringMVC Model attributes instance(used as a mapping method parameter) name */
  private static final String MODEL_ATTRIBUTE_METHDO_PARAMETER_NAME = "model";
  /** SpringMVC Model add attribute action names */
  private static final List<String> MODEL_ATTRIBUTE_ADD_ATTRIBUTE_ACTION_NAMES =
      Arrays.asList("put", "addAttribute", "addObject");

  private static final String SPRING_MODEL_AND_VIEW_TYPE_SIMPLE_NAME = "ModelAndView";

  private static final Predicate<CtMethod<?>> MODEL_AND_VIEW_MAPPING_METHOD_WITH_RESPONSE_BODY_PREDICATE =
      m -> m.getAnnotations().stream()
          .anyMatch(a -> a.getType().getSimpleName().equals("ResponseBody"));

  private static final Predicate<CtMethod<?>> MODEL_AND_VIEW_METHOD_RETURN_TYPE_PREDICATE =
      m -> SPRING_MODEL_AND_VIEW_TYPE_SIMPLE_NAME.equals(m.getType().getSimpleName());
  @SuppressWarnings("rawtypes")
  private static final Predicate<CtInvocation> MODEL_ADD_ATTRIBUTE_INVOCATION_PREDICATE =
      i -> MODEL_ATTRIBUTE_ADD_ATTRIBUTE_ACTION_NAMES.contains(i.getExecutable().getSimpleName());

  private final String appRootPackage;

  public MappingMethodExitMovementExtractor(String appRootPackage) {
    this.appRootPackage = Objects.requireNonNull(appRootPackage);
  }

  /**
   * Function that extracts the application domain entity names associated to a method local entity
   * instantiation from the provided method body.
   * 
   * The local instance is used to be pushed to the UI for a entity creation, so this instance will
   * not be considered as an exit data group movement, unless this local instance is used as simpler
   * for Object-Xml mapping (its type is annotated with "XmlRootElement" annotation)
   * 
   * The function returns a set of the application domain entity names.
   */
  private BiFunction<CtMethod<?>, String, Set<String>> extractAppEntityLocalInstanceVariableNamesFromMethodBody =
      (method, rootPackage) -> {

        Predicate<CtElement> constructorCallPredicate = e -> e instanceof CtConstructorCall;
        Predicate<CtConstructorCall<?>> localVariableInstantiationPredicate =
            cc -> cc.getParent() != null && cc.getParent() instanceof CtLocalVariable;
        Predicate<CtConstructorCall<?>> modelAndViewPredicate =
            cc -> SPRING_MODEL_AND_VIEW_TYPE_SIMPLE_NAME.equals(cc.getType().getSimpleName());
        Predicate<CtConstructorCall<?>> appEntityPredicate =
            cc -> cc.getType().getQualifiedName().startsWith(rootPackage);
        Predicate<CtConstructorCall<?>> objectXmlMappingEntityPredicate =
            cc -> cc.getExecutable().getDeclaringType().getTypeDeclaration().getAnnotations()
                .stream().anyMatch(a -> "XmlRootElement".equals(a.getType().getSimpleName()));

        return method.getBody().getElements(e -> e instanceof CtExpression).stream()
            .filter(constructorCallPredicate).map(e -> (CtConstructorCall<?>) e)
            .filter(localVariableInstantiationPredicate).filter(modelAndViewPredicate.negate())
            .filter(appEntityPredicate).filter(objectXmlMappingEntityPredicate.negate())
            .map(c -> ((CtLocalVariable<?>) c.getParent()).getSimpleName())
            .collect(Collectors.toSet());

      };


  /**
   * Function that extracts the exit data movements from the provided method. The method must have
   * the {@code @ResponseBody} method annotation.
   * 
   * This function is the implementation of the COSMIC Mapping Rule #09:
   * <p>
   * Each method having the annotation {@code @ResponseBody} and one of the annotations
   * {@code @PostMapping}, {@code @GetMapping} or {@code @RequestMapping}, is a method returning an
   * object of interest considered as a Exit data movement.
   * 
   * The function returns a set of the data movements.
   */
  private Function<CtMethod<?>, Set<DataMovement>> extractExitDataMovementForMappingMethodWithReponseBodyAnnotation =
      method -> {
        CtTypeReference<?> methodType = method.getType();
        if (methodType.getSimpleName().equals("String")) {
          CtStatement lastStatment = method.getBody().getLastStatement();
          if (!(lastStatment instanceof CtReturn)) {
            LOGGER.error("The last Statment is not an instance of CtReturn for the method '{}'",
                method.getSimpleName());
            return Collections.emptySet();
          }
          CtReturn<?> returnStatment = (CtReturn<?>) lastStatment;
          if (returnStatment.getReturnedExpression() instanceof CtInvocation) {
            CtInvocation<?> returnedExpression =
                (CtInvocation<?>) returnStatment.getReturnedExpression();
            String returnMethodName = returnedExpression.getExecutable().getSimpleName();
            String returnMethodArguments = returnedExpression.getArguments().isEmpty() ? ""
                : returnedExpression.getArguments().stream().map(e -> e.toString())
                    .collect(Collectors.joining(", ", "[", "]"));
            return Collections.singleton(
                new DataMovement(returnMethodName + returnMethodArguments, Movement.EXIT));
          }

          return Collections.singleton(
              new DataMovement(returnStatment.getReturnedExpression().toString(), Movement.EXIT));
        }

        boolean isIterableType =
            SourceCodeUtil.ITERABLE_TYPES.contains(methodType.getSimpleName().toUpperCase());
        if (isIterableType) {
          methodType = methodType.getActualTypeArguments().get(0);
        }
        String dataGroupName = String.format("%s%s", methodType.getSimpleName().toUpperCase(),
            isIterableType ? DATA_GROUP_ITERABLE_TYPE_NAME_SUFFIX : "");
        return Collections.singleton(new DataMovement(dataGroupName, Movement.EXIT));
      };

  /**
   * Function that extracts the exit data movements from the provided method.
   * 
   * If the provided method has SpringMVC Model attributes instance as entry parameter, the Exit
   * data movement info is extracted from a call of the "put" or "addAttribute" model actions,
   * otherwise it will be extracted from a call of the "addObject" action related to a local
   * instance of the type "ModelAndView".
   * 
   * The data movement need to be a type of an application domain entity witch the qualified name is
   * prefixed with {@code appRootPackage}.
   * 
   * This function is the implementation of the COSMIC Mapping Rule #08:
   * <p>
   * Each object of interest added as a new element to one of the instances {@code Map},
   * {@code Model}, {@code ModelMap} or {@code ModelAndView} in the body of a method having one of
   * the annotations {@code @ModelAttribute}, {@code @PostMapping}, {@code @GetMapping} or
   * {@code @RequestMapping}, is considered as an Exit data movement. The object of interest
   * instance must not be a local instance
   * 
   * 
   * The function returns a set of the data movements.
   */
  private BiFunction<CtMethod<?>, String, Set<DataMovement>> extractExitDataMovementsFromModelInstance =
      (method, rootPackage) -> {
        final Set<String> appEntityLocalInstanceVariableNames =
            extractAppEntityLocalInstanceVariableNamesFromMethodBody.apply(method, rootPackage);
        final Set<DataMovement> modelExitDataMovements =
            extractExitDatMovementsBasedOnModelInstanceAmongMethodEntryParameters(method,
                appEntityLocalInstanceVariableNames);
        final Set<DataMovement> modelAndViewExitDataMovements =
            extractExitDatMovementsBasedOnModelAndViewLocalInstanceInMethodBody(method,
                appEntityLocalInstanceVariableNames);
        final Set<DataMovement> localInvocationExits =
            extractExitDataMovementFromLocalInvocation(method);
        return Stream
            .concat(Stream.concat(modelExitDataMovements.stream(),
                modelAndViewExitDataMovements.stream()), localInvocationExits.stream())
            .collect(Collectors.toSet());
      };

  /**
   * Returns a set containing all of the Exit data movements in the body of the provided method.
   * 
   * 
   * @param method current method to analyze
   * @return a set containing all of the exit data movements in the provided method
   */
  public Set<DataMovement> extract(CtMethod<?> method) {

    if (MODEL_AND_VIEW_MAPPING_METHOD_WITH_RESPONSE_BODY_PREDICATE.test(method)) {
      return extractExitDataMovementForMappingMethodWithReponseBodyAnnotation.apply(method);
    }
    return extractExitDataMovementsFromModelInstance.apply(method, this.appRootPackage);
  }

  /**
   * Method that extracts the exit data movements associated to the named model attributes added to
   * the Model instance that binds a method parameter.
   * 
   * To add a new attribute to the model, the invocation call names are "put" and "addAttribute".
   * The data group name need to be an application domain entity name and it is not a method local
   * instance.
   * 
   * @param method CtMethod instance to analyze
   * @param appEntityLocalInstanceVariableNames set of the variable names related to the application
   *        domain entity considered as a method local instance
   * @return set of data movement
   */
  private Set<DataMovement> extractExitDatMovementsBasedOnModelInstanceAmongMethodEntryParameters(
      CtMethod<?> method, Set<String> appEntityLocalInstanceVariableNames) {
    boolean isModelAmongEntryParams = method.getParameters().stream()
        .filter(p -> p.getSimpleName().equals(MODEL_ATTRIBUTE_METHDO_PARAMETER_NAME))
        .anyMatch(p -> MODEL_ATTRIBUTE_TYPE_SIMPLE_NAMES.contains(p.getType().getSimpleName()));

    if (!isModelAmongEntryParams) {
      return Collections.emptySet();
    }

    return Optional.ofNullable(SourceCodeUtil.extracExecutableCallsFromMethodBody(method))
        .map(Collection::stream).orElseGet(Stream::empty)
        .filter(i -> i.getTarget() instanceof CtVariableRead)
        .filter(i -> ((CtVariableRead<?>) i.getTarget()).getVariable().getSimpleName()
            .equals(MODEL_ATTRIBUTE_METHDO_PARAMETER_NAME))
        .filter(MODEL_ADD_ATTRIBUTE_INVOCATION_PREDICATE)
        .map(i -> extractDataGroupFromModelAddAttributeInvocation(i,
            appEntityLocalInstanceVariableNames))
        .filter(Optional::isPresent).map(Optional::get).collect(Collectors.toSet());
  }

  /**
   * Method that extracts the exit data movements from the local invocation of the provided method.
   * 
   * The local invocation need to have the model instance among its entry parameters
   * 
   * @param method CtMethod instance
   * @return Set<DataMovement>
   */
  @SuppressWarnings("unchecked")
  private Set<DataMovement> extractExitDataMovementFromLocalInvocation(CtMethod<?> method) {

    final Optional<CtParameter<?>> modelEntryParam = method.getParameters().stream()
        .filter(p -> p.getSimpleName().equals(MODEL_ATTRIBUTE_METHDO_PARAMETER_NAME))
        .filter(p -> MODEL_ATTRIBUTE_TYPE_SIMPLE_NAMES.contains(p.getType().getSimpleName()))
        .findFirst();

    if (!modelEntryParam.isPresent()) {
      return Collections.emptySet();
    }

    final CtType<?> declaringController = method.getDeclaringType();

    // inner method that has a model as entry params
    // get main method parent type and get inner method based on method simple name type and params
    // extract exits from inner method body
    return Optional.ofNullable(SourceCodeUtil.extracExecutableCallsFromMethodBody(method))
        .map(Collection::stream).orElseGet(Stream::empty)
        .filter(i -> i.getArguments().stream()
            .anyMatch(a -> modelEntryParam.get().getSimpleName().equals(a.toString())))
        .map(i -> extractExitDataMovementFromControllerLocalInvocation(declaringController, i))
        .flatMap(Set::stream).collect(Collectors.toSet());
  }

  /**
   * Method that extracts the exit data movements associated to the model instance that is among the
   * provided invocation arguments.
   * 
   * @param controller current controller
   * @param invocation inner invocation
   * @return Set<DataMovement>
   */
  private Set<DataMovement> extractExitDataMovementFromControllerLocalInvocation(
      final CtType<?> controller, CtInvocation<?> invocation) {
    String methodName = invocation.getExecutable().getSimpleName();
    Optional<CtMethod<?>> privateMethod = controller.getAllMethods().stream()
        .filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst();
    if (!privateMethod.isPresent()) {
      return Collections.emptySet();
    }
    final Set<String> appEntityLocalInstanceVariableNames =
        extractAppEntityLocalInstanceVariableNamesFromMethodBody.apply(privateMethod.get(),
            this.appRootPackage);
    return extractExitDatMovementsBasedOnModelInstanceAmongMethodEntryParameters(
        privateMethod.get(), appEntityLocalInstanceVariableNames);
  }

  /**
   * Method that extracts the exit data movements associated to the named model attributes added to
   * the ModelAndView instance that binds a method return type.
   * 
   * To add a new attribute to the model, the invocation call name is "addObject". The data group
   * name need to be an application domain entity name and it is not a method local instance.
   * 
   * @param method CtMethod instance to analyze
   * @param appEntityLocalInstanceVariableNames set of the variable names related to the application
   *        domain entity considered as a method local instance
   * @return set of data movement
   */
  private Set<DataMovement> extractExitDatMovementsBasedOnModelAndViewLocalInstanceInMethodBody(
      CtMethod<?> method, Set<String> appEntityLocalInstanceVariableNames) {

    if (MODEL_AND_VIEW_METHOD_RETURN_TYPE_PREDICATE.negate().test(method)) {
      return Collections.emptySet();
    }
    return Optional.ofNullable(SourceCodeUtil.extracExecutableCallsFromMethodBody(method))
        .map(Collection::stream).orElseGet(Stream::empty)
        .filter(i -> i.getExecutable().getDeclaringType() != null)
        .filter(i -> SPRING_MODEL_AND_VIEW_TYPE_SIMPLE_NAME
            .equals(i.getExecutable().getDeclaringType().getSimpleName()))
        .filter(MODEL_ADD_ATTRIBUTE_INVOCATION_PREDICATE)
        .map(i -> extractDataGroupFromModelAddAttributeInvocation(i,
            appEntityLocalInstanceVariableNames))
        .filter(Optional::isPresent).map(Optional::get).collect(Collectors.toSet());
  }

  /**
   * Method that extracts the exit data movement from the provided model CtInvocation (Add model
   * attribute call).
   * 
   * We parse the invocation arguments (one or two arguments) to resolve the data group name. The
   * data group name need to be an application domain entity name and it is not a method local
   * instance.
   * 
   * @param modelInvocation Model CtInvocation instance to analyze
   * @param appEntityLocalInstanceVariableNames set of the variable names related to the application
   *        domain entity considered as a method local instance
   * @return Optional data movement
   */
  private Optional<DataMovement> extractDataGroupFromModelAddAttributeInvocation(
      CtInvocation<?> modelInvocation, Set<String> appEntityLocalInstanceVariableNames) {


    if (modelInvocation.getArguments().isEmpty() || modelInvocation.getArguments().size() > 2) {
      LOGGER.warn("Invalid model invocation argument size: signature={}, size={}",
          modelInvocation.getExecutable().getSignature(), modelInvocation.getArguments().size());
      return Optional.empty();
    }

    Object argumentValue =
        modelInvocation.getArguments().size() == 2 ? modelInvocation.getArguments().get(1)
            : modelInvocation.getArguments().get(0);

    if (argumentValue instanceof CtVariableRead) {
      return resolveDataMovementFromVariableReadType(appEntityLocalInstanceVariableNames,
          (CtVariableRead<?>) argumentValue);
    } else if (argumentValue instanceof CtInvocation) {
      return resolveDataMovementFromInvocationType(((CtInvocation<?>) argumentValue));
    }
    return Optional.empty();
  }


  private Optional<DataMovement> resolveDataMovementFromInvocationType(
      CtInvocation<?> argumentValue) {
    CtTypeReference<?> callReturnType = argumentValue.getType();
    if (callReturnType == null) {
      // If DeclaringType is null that means the exec ref can be a method from a spring data
      // repository interface
      final CtExecutableReference<?> executableRef = argumentValue.getExecutable();
      try {
        final CtTypeReference<?> typeRef =
            ((CtFieldRead<?>) ((CtInvocation<?>) executableRef.getParent()).getTarget())
                .getVariable().getType().getSuperInterfaces().iterator().next();
        final String methodName = executableRef.getSimpleName();
        final String domainEntity =
            typeRef.getActualTypeArguments().get(0).getSimpleName().toUpperCase();
        return Optional.of(new DataMovement(
            String.format("%s%s", domainEntity,
                methodName.startsWith("findAll") ? DATA_GROUP_ITERABLE_TYPE_NAME_SUFFIX : ""),
            Movement.EXIT));
      } catch (Exception e) {
        LOGGER.info("Cannot resolve the data group name for the invocation :: signature={}",
            executableRef.getSignature());
      }
    } else {
      CtTypeReference<?> argType = callReturnType;
      boolean isIterableType =
          SourceCodeUtil.ITERABLE_TYPES.contains(callReturnType.getSimpleName().toUpperCase());
      if (isIterableType) {
        argType = callReturnType.getActualTypeArguments().get(0);
      }
      if (SourceCodeUtil.APPLICATION_ENTITY_TYPEREFERENCE_PREDICATE.test(this.appRootPackage,
          argType)) {
        String dataGroupName = String.format("%s%s", argType.getSimpleName().toUpperCase(),
            isIterableType ? DATA_GROUP_ITERABLE_TYPE_NAME_SUFFIX : "");
        return Optional.of(new DataMovement(dataGroupName, Movement.EXIT));
      }
    }
    return Optional.empty();
  }


  private Optional<DataMovement> resolveDataMovementFromVariableReadType(
      Set<String> appEntityLocalInstanceVariableNames, CtVariableRead<?> arg) {
    BiPredicate<CtVariableRead<?>, Set<String>> isLocalVariablePredicate =
        (argument, appBeanLocalInstanceVariableNames) -> appBeanLocalInstanceVariableNames
            .contains(argument.getVariable().getSimpleName());
    CtTypeReference<?> argType =
        arg.getType() == null ? arg.getVariable().getType() : arg.getType();
    boolean isIterableType =
        SourceCodeUtil.ITERABLE_TYPES.contains(argType.getSimpleName().toUpperCase());
    if (isIterableType) {
      argType = argType.getActualTypeArguments().get(0);
    }
    if (SourceCodeUtil.APPLICATION_ENTITY_TYPEREFERENCE_PREDICATE.test(this.appRootPackage, argType)
        && isLocalVariablePredicate.negate().test(arg, appEntityLocalInstanceVariableNames)) {
      String dataGroupName = String.format("%s%s", argType.getSimpleName().toUpperCase(),
          isIterableType ? DATA_GROUP_ITERABLE_TYPE_NAME_SUFFIX : "");
      return Optional.of(new DataMovement(dataGroupName, Movement.EXIT));
    }
    return Optional.empty();
  }
}
