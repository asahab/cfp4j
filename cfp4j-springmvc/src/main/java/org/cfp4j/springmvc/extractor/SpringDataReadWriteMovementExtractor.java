package org.cfp4j.springmvc.extractor;

import static org.cfp4j.springmvc.util.SourceCodeUtil.DATA_GROUP_ITERABLE_TYPE_NAME_SUFFIX;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.cfp4j.entity.Movement;
import org.cfp4j.springmvc.model.DataMovement;
import org.cfp4j.springmvc.util.SourceCodeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spoon.reflect.code.CtFieldRead;
import spoon.reflect.code.CtInvocation;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.reference.CtExecutableReference;
import spoon.reflect.reference.CtTypeReference;

/**
 * A class representing the logic implementation to extract the COSMIC Function Points related to
 * the READ_WRITE data movements. The targeted classes are the classes that implement a spring
 * repository interface.
 */

public class SpringDataReadWriteMovementExtractor {


  private static final Logger LOGGER =
      LoggerFactory.getLogger(SpringDataReadWriteMovementExtractor.class);
  /** Method name prefix that indicates a data group collection read */
  private static final String SPRING_DATA_JPA_REPOSITORY_COLLECTION_READ_METHOD_NAME_PREFIX =
      "findAll";
  /** Spring Data JPARepository flush method name */
  private static final String SPRING_DATA_JPA_REPOSITORY_FLUSH_METHOD_NAME = "flush";
  /** list of the spring data repository interface qualified names */
  private static final List<String> SPRING_DATA_REPOSITORY_QUALIFIED_NAMES =
      Arrays.asList("org.springframework.data.repository.Repository",
          "org.springframework.data.jpa.repository.JpaRepository",
          "org.springframework.data.repository.CrudRepository",
          "org.springframework.data.repository.PagingAndSortingRepository");
  /**
   * List of repository method names provided by Spring data that represent a data read (select
   * query) movement
   */
  private static final List<String> SPRING_DATA_REPOSITORY_READ_METHOD_NAMES = Arrays.asList(
      "findById", "existsById", SPRING_DATA_JPA_REPOSITORY_COLLECTION_READ_METHOD_NAME_PREFIX,
      "findAllById", "getOne", "count");
  /**
   * List of repository method names provided by Spring data that represent a data write (insert,
   * update and delete queries) movement
   */
  private static final List<String> SPRING_DATA_REPOSITORY_WRITE_METHOD_NAMES =
      Arrays.asList("save", "saveAll", "saveAndFlush", "deleteInBatch", "deleteAllInBatch",
          "deleteById", "delete", "deleteAll");

  /**
   * Predicate that is used to test if an instance of CtTypeReference is a child of a spring data
   * repository
   */
  private static final Predicate<CtTypeReference<?>> SPRING_DATA_REPOSITORY_CHILD_PREDICATE =
      t -> t.getSuperInterfaces().stream()
          .anyMatch(i -> SPRING_DATA_REPOSITORY_QUALIFIED_NAMES.contains(i.getQualifiedName()));
  /**
   * Predicate that is used to test if an instance of CtInvocation doesn't have a return type (void)
   */
  private static final Predicate<CtInvocation<?>> INVOCATION_WITH_VOID_TYPE_PREDICATE =
      i -> i.getType().getSimpleName().equals("void");



  /**
   * Function that looks for the Spring Data Repository method calls from the provided method. The
   * Spring Data Repository invocation lookup process will parse the method body and the inner
   * invocation bodies in a recursive way.
   * 
   * The look up for the inner invocation is done from the provided appCtMethods Map.
   * 
   * 
   * The function returns a set of the repository invocations.
   */
  private static BiFunction<Map<String, CtMethod<?>>, CtInvocation<?>, Set<CtInvocation<?>>> lookForRepositoryInvocationCalls =
      (appCtMethods, invocation) -> {

        final CtExecutableReference<?> executableRef = invocation.getExecutable();

        // If DeclaringType is null that means the exec ref can be a method from a spring data
        // repository interface)
        CtTypeReference<?> typeRef = executableRef.getDeclaringType();
        if (typeRef == null) {
          try {
            typeRef = ((CtFieldRead<?>) ((CtInvocation<?>) executableRef.getParent()).getTarget())
                .getVariable().getType();
          } catch (Exception e) {
            LOGGER.debug("Invalid Invocation Method :: signature={}", executableRef.getSignature());
            return Collections.emptySet();
          }
        }
        if (SPRING_DATA_REPOSITORY_CHILD_PREDICATE.test(typeRef)) {
          LOGGER.debug("Repository Invocation Method :: declaring_type={}, signature={}",
              typeRef.getQualifiedName(), executableRef.getSignature());
          return Collections.singleton(invocation);
        }

        CtMethod<?> typeMethod = appCtMethods
            .get(String.format("%s.%s", typeRef.getQualifiedName(), executableRef.getSignature()));

        if (typeMethod == null)
          return Collections.emptySet();

        @SuppressWarnings("rawtypes")
        Set<CtInvocation> innerInvocationCalls =
            SourceCodeUtil.extracExecutableCallsFromMethodBody(typeMethod);

        if (innerInvocationCalls.isEmpty())
          return Collections.emptySet();

        return innerInvocationCalls.stream()
            .map(i -> SpringDataReadWriteMovementExtractor.lookForRepositoryInvocationCalls
                .apply(appCtMethods, i))
            .flatMap(Set::stream).collect(Collectors.toSet());
      };



  /**
   * Function that extracts the Read data movement from the provided {@code CtExecutableReference}
   * representing a Spring Data Repository invocation (or method).
   * 
   * 
   * If the method name is among of the defined Spring Data framework that presents a read method,
   * then the Function returns an Optional instance with a Read data movement instance, otherwise an
   * empty Option instance will be returned.
   * 
   * If the method name starts with the string "findAll", the data group name is the declared
   * repository domain entity name concatenated with the string "_LIST".
   * 
   * This function is the implementation of the COSMIC Mapping Rule #10:
   * <p>
   * Each call to one of the "findById", "existsById", "findAll", "getOne", "count" or "findAllById"
   * methods of a Spring Data Repository class, recursively from a method with one of the
   * annotations {@code @ModelAttribute}, {@code @PostMapping}, {@code @GetMapping} or
   * {@code @RequestMapping}, is considered as a Read data movement.
   * 
   */
  private static Function<CtExecutableReference<?>, Optional<DataMovement>> extractReadDataGroupFromPredefinedRepositoryInvocation =
      executableRef -> {

        final String methodName = executableRef.getSimpleName();
        boolean isRepositoryReadMethod =
            SPRING_DATA_REPOSITORY_READ_METHOD_NAMES.stream().anyMatch(methodName::equals);
        if (isRepositoryReadMethod) {
          CtTypeReference<?> typeRef =
              ((CtFieldRead<?>) ((CtInvocation<?>) executableRef.getParent()).getTarget())
                  .getVariable().getType().getSuperInterfaces().iterator().next();
          String domainEntity =
              typeRef.getActualTypeArguments().get(0).getSimpleName().toUpperCase();
          return Optional.of(new DataMovement(String.format("%s%s", domainEntity,
              methodName.startsWith(SPRING_DATA_JPA_REPOSITORY_COLLECTION_READ_METHOD_NAME_PREFIX)
                  ? DATA_GROUP_ITERABLE_TYPE_NAME_SUFFIX
                  : ""),
              Movement.READ));
        }
        return Optional.empty();
      };

  /**
   * Function that extracts the Write data movement from the provided {@code CtExecutableReference}
   * representing a Spring Data Repository invocation (or method).
   * 
   * 
   * If the method name is among of the defined Spring Data framework that presents a persist
   * method, then the Function returns an Optional instance with a Write data movement instance,
   * otherwise an empty Option instance will be returned.
   * 
   * 
   * This function is the implementation of the COSMIC Mapping Rule #11:
   * <p>
   * Each call to one of the methods "save", "saveAll", "saveAndFlush", "deleteInBatch",
   * "deleteAllInBatch", "deleteById", "delete" or "deleteAll" of a Spring Data Repository class,
   * recursively from a method having one of the annotations {@code @ModelAttribute},
   * {@code @PostMapping}, {@code @GetMapping} or {@code @RequestMapping}, is considered as a Write
   * data movement.
   * 
   */
  private static Function<CtExecutableReference<?>, Optional<DataMovement>> extractWriteDataGroupFromPredefinedRepositoryInvocation =
      executableRef -> {

        final String methodName = executableRef.getSimpleName();
        boolean isRepositoryWriteMethod =
            SPRING_DATA_REPOSITORY_WRITE_METHOD_NAMES.stream().anyMatch(methodName::equals);
        if (isRepositoryWriteMethod) {
          CtTypeReference<?> typeRef =
              ((CtFieldRead<?>) ((CtInvocation<?>) executableRef.getParent()).getTarget())
                  .getVariable().getType().getSuperInterfaces().iterator().next();
          String domainEntity =
              typeRef.getActualTypeArguments().get(0).getSimpleName().toUpperCase();
          return Optional.of(new DataMovement(domainEntity, Movement.WRITE));
        }
        return Optional.empty();
      };
  /**
   * Function that extracts the data movement from the provided custom repository method. A custom
   * method is a method that was defined in the current source code and not a override spring data
   * repository method.
   * 
   * If the provided invocation does not have a return type (it is a void type), so this invocation
   * is considered a write movement, the data group name is the method argument expression.
   * 
   * The invocation that has return type, it is considered a read movement, the data group name is
   * the type simple name for non generic return type, otherwise if the generic type is an iterator
   * type, the data group is the concatenation of the type simple name and the string "_LIST".
   * 
   * This function is the implementation of the COSMIC Mapping Rule #12:
   * <p>
   * Each call to a custom method of a class implementing the Spring Data Repository interface,
   * recursively from a method having one of the annotations {@code @ModelAttribute},
   * {@code @PostMapping}, {@code @GetMapping} or {@code @RequestMapping}, is considered a data
   * movement. If this method has a return type then it is a Read data movement type is in "Read",
   * otherwise (void type), it is a Write data movement.
   * 
   */
  private static Function<CtInvocation<?>, DataMovement> extractDataGroupFromRepositoryCustomInvocation =
      i -> {
        if (INVOCATION_WITH_VOID_TYPE_PREDICATE.test(i)) {
          // Extract the method arguments, the data group name is the argument names
          Set<String> argNames = i.getArguments().stream()
              .map(e -> e.toString().replace(".get", "_").replace("()", "").toUpperCase())
              .collect(Collectors.toSet());
          return new DataMovement(
              argNames.size() == 1 ? argNames.iterator().next() : argNames.toString(),
              Movement.WRITE);
        }

        if (i.getExecutable().getType().isPrimitive()) {
          return new DataMovement(i.getExecutable().getSimpleName().toUpperCase(), Movement.READ);
        }

        if (i.getExecutable().getType().getActualTypeArguments().isEmpty()) {
          return new DataMovement(i.getType().getSimpleName().toUpperCase(), Movement.READ);
        }


        String typeName = i.getExecutable().getType().getSimpleName();
        boolean isIterableType = SourceCodeUtil.ITERABLE_TYPES.contains(typeName.toUpperCase());
        String dataGroupName = String.format("%s%s",
            i.getExecutable().getType().getActualTypeArguments().get(0).getSimpleName(),
            isIterableType ? DATA_GROUP_ITERABLE_TYPE_NAME_SUFFIX : "");
        return new DataMovement(dataGroupName.toUpperCase(), Movement.READ);
      };


  /**
   * Function that extracts the data movement from the provided repository invocation (or method).
   * 
   * The method with name "flush" is not considered as a data movement(this method is used just to
   * refresh the JPA session). Based on the method name, we select the type of the data movement for
   * the method is among the methods provided by Spring Data framework, otherwise the method is
   * considered as a custom method (see the Function
   * extractDataGroupFromRepositoryCustomInvocation).
   * 
   * The data movement name is the simple name of the declared repository domain entity name.
   * 
   */
  private static Function<CtInvocation<?>, Optional<DataMovement>> extractDataGroupFromRepositoryInvocation =
      i -> {

        final CtExecutableReference<?> executableRef = i.getExecutable();
        final String methodName = executableRef.getSimpleName();

        if (methodName.equals(SPRING_DATA_JPA_REPOSITORY_FLUSH_METHOD_NAME))
          return Optional.empty();

        final Optional<DataMovement> readDataMovement =
            extractReadDataGroupFromPredefinedRepositoryInvocation.apply(executableRef);
        if (readDataMovement.isPresent()) {
          return readDataMovement;
        }
        final Optional<DataMovement> writeDataMovement =
            extractWriteDataGroupFromPredefinedRepositoryInvocation.apply(executableRef);
        if (writeDataMovement.isPresent()) {
          return writeDataMovement;
        }
        return Optional.of(extractDataGroupFromRepositoryCustomInvocation.apply(i));
      };



  /**
   * Returns a set containing all of the Read_Write data movements in the body of the provided
   * method.
   * 
   * The data movement will be extracted from the Spring Data Repository invocations.
   * 
   * 
   * @param appCtMethods map of the CtMethod instances of the analyzed source code
   * @param method current method to analyze
   * @return an set containing all of the data movements in the provided method
   */
  public Set<DataMovement> extract(Map<String, CtMethod<?>> appCtMethods, CtMethod<?> method) {

    return Optional.ofNullable(SourceCodeUtil.extracExecutableCallsFromMethodBody(method))
        .map(Collection::stream).orElseGet(Stream::empty)
        .map(i -> lookForRepositoryInvocationCalls.apply(appCtMethods, i)).flatMap(Set::stream)
        .map(extractDataGroupFromRepositoryInvocation).filter(Optional::isPresent)
        .map(Optional::get).collect(Collectors.toSet());
  }

  /**
   * BiPredicate that checks if at least a delete invocation is called from the provided method. The
   * check process will parse the method body and the inner invocation bodies in a recursive way.
   * 
   * The check from the inner invocation is done from the provided appCtMethods Map.
   * 
   */
  public static final BiPredicate<Map<String, CtMethod<?>>, CtMethod<?>> DELETE_REPOSITORY_INVOCATIONS_EXISTS_PREDICATE =
      (appCtMethods, method) -> Optional
          .ofNullable(SourceCodeUtil.extracExecutableCallsFromMethodBody(method))
          .map(Collection::stream).orElseGet(Stream::empty)
          .map(i -> lookForRepositoryInvocationCalls.apply(appCtMethods, i)).flatMap(Set::stream)
          .filter(i -> i.getExecutable() != null)
          .anyMatch(i -> i.getExecutable().getSimpleName().startsWith("delete"));

}
