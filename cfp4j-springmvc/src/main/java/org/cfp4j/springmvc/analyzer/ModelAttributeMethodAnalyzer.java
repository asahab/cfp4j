package org.cfp4j.springmvc.analyzer;

import static org.cfp4j.springmvc.util.SourceCodeUtil.DATA_GROUP_ITERABLE_TYPE_NAME_SUFFIX;
import static org.cfp4j.springmvc.util.SourceCodeUtil.ITERABLE_TYPES;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.cfp4j.entity.Movement;
import org.cfp4j.springmvc.extractor.MappingMethodEntryMovementExtractor;
import org.cfp4j.springmvc.extractor.MappingMethodExitMovementExtractor;
import org.cfp4j.springmvc.extractor.SpringDataReadWriteMovementExtractor;
import org.cfp4j.springmvc.model.DataMovement;

import spoon.reflect.declaration.CtMethod;

/**
 * A class representing the analysis of a mapping method to extract the data movements. 
 * The extraction is based on the entry parameters and the method body content.
 * The method to analyze need to be annotated with the spring method annotation "ModelAttribute".
 * 
 */
public class ModelAttributeMethodAnalyzer {

	private final MappingMethodEntryMovementExtractor mappingMethodEntryMovementExtractor;
	private final MappingMethodExitMovementExtractor mappingMethodExitMovementExtractor;
	private final SpringDataReadWriteMovementExtractor springDataReadWriteMovementExtractor;

	public ModelAttributeMethodAnalyzer(String appRootPackage) {
		this.mappingMethodEntryMovementExtractor = new MappingMethodEntryMovementExtractor(appRootPackage);
		this.mappingMethodExitMovementExtractor = new MappingMethodExitMovementExtractor(appRootPackage);
		this.springDataReadWriteMovementExtractor = new SpringDataReadWriteMovementExtractor();
	}

	/**
	 * Function that analyze the method return type.
	 * As the return type can be a method local instance (initialize a creation -> return new instance), 
	 * the return type will be considered as exit if it was read or write from a repository,
	 * otherwise the function returns an empty option.
	 * 
	 * If the provided method return type is {@code Void}, the function returns an empty Optional instance.
	 * 
	 */
	private BiFunction<CtMethod<?>, Set<DataMovement>, Optional<DataMovement>>  extractReturnTypeFunction = (modelAttributeMethod,readWrites) -> {
		if (modelAttributeMethod.getType().getSimpleName().equals("void")){
			return Optional.empty();
		}

		Set<String> readWriteDataNames = readWrites.stream().map(dm->dm.getName()).collect(Collectors.toSet());
		
		if (modelAttributeMethod.getType().getActualTypeArguments().isEmpty()){
			String dataName = modelAttributeMethod.getType().getSimpleName().toUpperCase();
			return readWriteDataNames.contains(dataName) ? Optional.of(new DataMovement(dataName, Movement.EXIT)) : Optional.empty();
		}
		
		String typeName = modelAttributeMethod.getType().getSimpleName();
		boolean isIterableType = ITERABLE_TYPES.contains(typeName.toUpperCase());
		String dataGroupName = String.format("%s%s",
				modelAttributeMethod.getType().getActualTypeArguments().get(0).getSimpleName(),
				isIterableType ? DATA_GROUP_ITERABLE_TYPE_NAME_SUFFIX : "").toUpperCase();
		
		return readWriteDataNames.contains(dataGroupName) ? Optional.of(new DataMovement(dataGroupName, Movement.EXIT)) : Optional.empty();
	};
	
	/**
	 * Main method that return set of DatMovement instances that is results of the analysis of the method entry parameters and its body.
	 * 
	 * @param modelAttributeMethod method to analyze
	 * @param appCtMethods map of the CtMethod instances of the analyzed source code 
	 * @return Set<DataMovement>
	 */
	public Set<DataMovement> extract(CtMethod<?> modelAttributeMethod, Map<String, CtMethod<?>> appCtMethods) {

		final Set<DataMovement> entries = this.mappingMethodEntryMovementExtractor.extract(modelAttributeMethod);
		final Set<DataMovement> exits = this.mappingMethodExitMovementExtractor.extract(modelAttributeMethod);
		final Set<DataMovement> readWrites = this.springDataReadWriteMovementExtractor.extract(appCtMethods, modelAttributeMethod);

		Optional<DataMovement>  returnType = extractReturnTypeFunction.apply(modelAttributeMethod, readWrites);
		if (returnType.isPresent()){
			exits.add(returnType.get());
		}
		return Stream.concat(Stream.concat(entries.stream(), exits.stream()), readWrites.stream()).collect(Collectors.toSet());
	}
	

}
