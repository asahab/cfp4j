package org.cfp4j.springmvc.analyzer;

import static org.cfp4j.springmvc.extractor.SpringDataReadWriteMovementExtractor.DELETE_REPOSITORY_INVOCATIONS_EXISTS_PREDICATE;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.validation.Valid;
import org.cfp4j.entity.DataGroup;
import org.cfp4j.entity.FunctionalProcess;
import org.cfp4j.entity.Movement;
import org.cfp4j.springmvc.extractor.MappingMethodEntryMovementExtractor;
import org.cfp4j.springmvc.extractor.MappingMethodExitMovementExtractor;
import org.cfp4j.springmvc.extractor.SpringDataReadWriteMovementExtractor;
import org.cfp4j.springmvc.model.DataMovement;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtParameter;

/**
 * A class representing the analysis of a mapping method to extract the data movements. The
 * extraction is based on the entry parameters and the method body content. The method to analyze
 * need to be annotated with the following spring annotation:
 * ["RequestMapping","GetMapping","PostMapping"].
 * 
 */
public class MappingMethodAnalyzer {

  private static final String MESSAGE_DATA_GROUP_NAME = "MESSAGE";
  private final MappingMethodEntryMovementExtractor mappingMethodEntryMovementExtractor;
  private final MappingMethodExitMovementExtractor mappingMethodExitMovementExtractor;
  private final SpringDataReadWriteMovementExtractor springDataReadWriteMovementExtractor;

  public MappingMethodAnalyzer(String appRootPackage) {
    this.mappingMethodEntryMovementExtractor =
        new MappingMethodEntryMovementExtractor(appRootPackage);
    this.mappingMethodExitMovementExtractor =
        new MappingMethodExitMovementExtractor(appRootPackage);
    this.springDataReadWriteMovementExtractor = new SpringDataReadWriteMovementExtractor();
  }

  /**
   * 
   * Function that exclude the provided {@code modelAttributMethodExits} from the provided data
   * movement set.
   * 
   * This function is the implementation of the COSMIC Mapping Rule #05:
   * <p>
   * Each entry data movement related to method having one of the annotations {@code @PostMapping},
   * {@code @GetMapping} or {@code @RequestMapping} must not be an exit data movement related to a
   * method having the annotation {@code @ModelAttribute}
   * 
   * The function returns a set of DataMovement instances.
   * 
   */
  private static BiFunction<Set<DataMovement>, Set<String>, Set<DataMovement>> excludeModelAttributMethodExitDataMovements =
      (dataMovements, modelAttributMethodExits) -> Optional.ofNullable(dataMovements)
          .map(Collection::stream).orElseGet(Stream::empty)
          .filter(dt -> !modelAttributMethodExits.contains(dt.getName()))
          .collect(Collectors.toSet());

  /**
   * 
   * Function that returns an optional instance including an entry data movement representing a
   * functional user trigger if it is applied, otherwise the return will be an empty Optional
   * instance. The FU and the data that initiate a FP aren't always obvious, however according to
   * the COSMIC rules, there must be a FU and an Entry (otherwise the result isn't a COSMIC
   * measurement).
   * 
   * This function is the implementation of the COSMIC Mapping Rule #06:
   * <p>
   * If at least one Read or Write data movement has been recorded, and no Entry data movement has
   * been associated to a method having one of the annotations {@code @PostMapping},
   * {@code @GetMapping} or {@code @RequestMapping}, then an Entry data movement a trigger for this
   * functional process must be added.
   * 
   * The function returns an Optional instance.
   * 
   */
  private static BiFunction<Set<DataMovement>, Set<DataMovement>, Optional<DataMovement>> extractOptionalFonctionalUserTrigger =
      (entries, readwrites) -> {
        /*
         * The FU and the data that initiate a FP aren't always obvious, however according to the
         * COSMIC rules, there must be a FU and an Entry (otherwise the result isn't a COSMIC
         * measurement).
         */
        final BiPredicate<Set<DataMovement>, Set<DataMovement>> triggerPredicate =
            (e, rw) -> (e.isEmpty() && !rw.isEmpty());
        return triggerPredicate.test(entries, readwrites)
            ? Optional.of(new DataMovement("TRIGGER", Movement.ENTRY))
            : Optional.empty();
      };

  /**
   * Function that returns an optional instance including an entry data movement representing a
   * functional user message if it exists, otherwise the return will be an empty Optional instance.
   * 
   * This function is the implementation of the COSMIC Mapping Rule #07:
   * <p>
   * If a method having one of the annotations {@code @PostMapping}, {@code @GetMapping} or
   * {@code @RequestMapping}, has at least one input parameter annotated with the annotation
   * {@code @Valid} or is {@code BindingResult} instance or there is a call to a "Delete" method
   * during the execution of this method, then an Exit data movement representing an output message
   * related to an input parameter validation message or a deletion confirmation message.
   * 
   * returned. The function returns an Optional instance.
   */
  private BiFunction<Map<String, CtMethod<?>>, CtMethod<?>, Optional<DataMovement>> extractValidationOrConfirmationMessageDataGroup =
      (appCtMethods, method) -> {
        List<CtParameter<?>> entryParams = method.getParameters();
        final boolean isValidAnnotationExists =
            Optional.ofNullable(entryParams).map(Collection::stream).orElseGet(Stream::empty)
                .anyMatch(p -> p.hasAnnotation(Valid.class));
        final boolean isBindingResultExists =
            Optional.ofNullable(entryParams).map(Collection::stream).orElseGet(Stream::empty)
                .anyMatch(p -> "BindingResult".equals(p.getType().getSimpleName()));

        if (isValidAnnotationExists || isBindingResultExists) {
          return Optional.of(new DataMovement(MESSAGE_DATA_GROUP_NAME, Movement.EXIT));
        }

        if (DELETE_REPOSITORY_INVOCATIONS_EXISTS_PREDICATE.test(appCtMethods, method)) {
          return Optional.of(new DataMovement(MESSAGE_DATA_GROUP_NAME, Movement.EXIT));
        }
        return Optional.empty();
      };

  /**
   * Main method that returns an instance of {@code FunctionalProcess} type that is results of the
   * analysis of the method entry parameters and its body.
   * 
   * The implementation includes the COSMIC rule below:
   * <p>
   * The FU and the data that initiate a FP aren’t always obvious, however according to the COSMIC
   * rules, there must be a FU and an Entry (otherwise the result isn’t a COSMIC measurement).
   * </p>
   * 
   * @param controllerMethod method to analyze
   * @param appCtMethods map of the CtMethod instances of the analyzed source code
   * @param modelAttributMethodExits set of the parameter to exclude from the final result.
   * @return FunctionalProcess
   */
  public FunctionalProcess analyze(CtMethod<?> controllerMethod,
      Map<String, CtMethod<?>> appCtMethods, Set<String> modelAttributMethodExits) {
    final FunctionalProcess functionalProcess =
        new FunctionalProcess(controllerMethod.getSimpleName());
    // parse entry parameters to get the message data group if there is any
    Optional<DataMovement> messageDataGroup =
        extractValidationOrConfirmationMessageDataGroup.apply(appCtMethods, controllerMethod);
    if (messageDataGroup.isPresent()) {
      functionalProcess.getDataGroupSet().add(
          new DataGroup(messageDataGroup.get().getName(), messageDataGroup.get().getMovement()));
    }
    // extract entry data groups
    Set<DataMovement> entries = this.mappingMethodEntryMovementExtractor.extract(controllerMethod);
    // exclude the Model Attribute Method exit data movements from the method entries
    entries = excludeModelAttributMethodExitDataMovements.apply(entries, modelAttributMethodExits);
    // extract exit data groups
    Set<DataMovement> exits = this.mappingMethodExitMovementExtractor.extract(controllerMethod);
    // check read | write using spring data == extract interfaces that extend Repository or
    // JpaRepository types
    Set<DataMovement> readWrites =
        this.springDataReadWriteMovementExtractor.extract(appCtMethods, controllerMethod);

    // extract trigger entry
    Optional<DataMovement> trigger =
        extractOptionalFonctionalUserTrigger.apply(entries, readWrites);
    if (trigger.isPresent()) {
      functionalProcess.getDataGroupSet()
          .add(new DataGroup(trigger.get().getName(), trigger.get().getMovement()));
    }

    functionalProcess.getDataGroupSet()
        .addAll(Stream.concat(Stream.concat(entries.stream(), exits.stream()), readWrites.stream())
            .collect(Collectors.groupingBy(DataMovement::getName,
                Collectors.mapping(DataMovement::getMovement, Collectors.toSet())))
            .entrySet().stream().map(e -> new DataGroup(e.getKey(), e.getValue()))
            .collect(Collectors.toSet()));
    return functionalProcess;
  }



}
