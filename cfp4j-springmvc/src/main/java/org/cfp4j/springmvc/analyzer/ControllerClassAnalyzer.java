package org.cfp4j.springmvc.analyzer;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.cfp4j.entity.FunctionalProcess;
import org.cfp4j.entity.Movement;
import org.cfp4j.entity.Reference;
import org.cfp4j.springmvc.model.DataMovement;

import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtType;

/**
 * A class representing the analysis of a class source code to extract the data movements. 
 * The class to analyze need to be annotated with the spring method annotation "Controller".
 * 
 */
public class ControllerClassAnalyzer{
	
	/** 
	 * List of mapping annotation simple names
	 * Using the simple name instead of qualified name in case the import statement contains '*' like org.springframework.web.bind.annotation.*;
	 * because in this case the the default package will be the current class package
	 */
	private static final List<String> MODEL_AND_VIEW_MAPPING_METHOD_ANNOTATIONS = Arrays.asList("RequestMapping", "GetMapping", "PostMapping");
	
	
	private final MappingMethodAnalyzer mappingMethodAnalyzer;
	private final ModelAttributeMethodAnalyzer modelAndViewMethodAnalyzer;
	
	public ControllerClassAnalyzer(String appRootPackage) {
		this.mappingMethodAnalyzer = new MappingMethodAnalyzer(appRootPackage);
		this.modelAndViewMethodAnalyzer = new ModelAttributeMethodAnalyzer(appRootPackage);
	}

	/**
	 * Main method that returns a {@code Reference} instance that represents the result of the analysis of the provided controller class.
	 * 
	 * @param controllerType CtType instance of a controller class
	 * @param appTypeMethodCache map of the CtMethod instances of the analyzed source code 
	 * @return Reference
	 */
	public Reference analyze(CtType<?> controllerType, Map<String, CtMethod<?>> appTypeMethodCache) {
		final Set<DataMovement> modelAttributeMethodDataMovements = retrieveModelAttributeMethodDataGroups(controllerType, appTypeMethodCache);
		final Set<String> modelAttributMethodExits = modelAttributeMethodDataMovements.stream()
                .filter(dt->dt.getMovement().equals(Movement.EXIT))
                .map(DataMovement::getName)
                .collect(Collectors.toSet());
		final Set<FunctionalProcess> functionalProcesses =
				controllerType.getAllMethods()
				.stream()
				.filter(MODEL_AND_VIEW_MAPPING_METHOD_ANNOTATION_PREDICATE)
				.map(ctMethod -> {
                    final FunctionalProcess functionalProcess = this.mappingMethodAnalyzer.analyze(ctMethod, appTypeMethodCache,modelAttributMethodExits);
					modelAttributeMethodDataMovements.forEach(dm -> functionalProcess.addDataGroupMovement(dm.getName(), dm.getMovement()));
                    return functionalProcess;
                })
                .filter(FUNTIONAL_PROCCESS_VALIDATION_PREDICATE)
				.collect(Collectors.toSet());

        return new Reference(controllerType.getSimpleName(), functionalProcesses);
	}
	
	/**
	 * Extract the data movements from the Model Attribute Methods(method annotated with @ModelAttribute) declared in the provided controller class.
	 * 
	 * @param controllerType CtType instance of a controller class
	 * @param appTypeMethodCache map of the CtMethod instances of the analyzed source code 
	 * @return Set<DataMovement>
	 */
	private Set<DataMovement> retrieveModelAttributeMethodDataGroups(CtType<?> controllerType, Map<String, CtMethod<?>> appTypeMethodCache) {
        final Function<CtMethod<?>, Set<DataMovement>> modelAttributeMethodFP = ctMethod -> this.modelAndViewMethodAnalyzer.extract(ctMethod,appTypeMethodCache);
        return controllerType.getAllMethods().stream()
				        .filter(MODEL_ATTRIBUTE_METHOD_PREDICATE)
                        .map(modelAttributeMethodFP)
						.flatMap(Set::stream)
                        .collect(Collectors.toSet());

	}

	private static final Predicate<CtMethod<?>> MODEL_ATTRIBUTE_METHOD_PREDICATE =m -> m.getAnnotations().stream().anyMatch(a -> a.getType().getSimpleName().equals("ModelAttribute"));
	private static final Predicate<FunctionalProcess> FUNTIONAL_PROCCESS_VALIDATION_PREDICATE = fp -> fp.functionalPoints() >= 2;
	private static final Predicate<CtMethod<?>> MODEL_AND_VIEW_MAPPING_METHOD_ANNOTATION_PREDICATE = m -> m.getAnnotations().stream().anyMatch(a -> MODEL_AND_VIEW_MAPPING_METHOD_ANNOTATIONS.contains(a.getType().getSimpleName()));
}
