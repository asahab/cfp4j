package org.cfp4j.springmvc.model;

import org.cfp4j.entity.Movement;

import java.util.Objects;

public class DataMovement {

    private final String name;
    private final Movement movement;

    public DataMovement(String name, Movement movement) {
        this.name = Objects.requireNonNull(name);
        this.movement = Objects.requireNonNull(movement);
    }

    public String getName() {
        return name;
    }

    public Movement getMovement() {
        return movement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataMovement that = (DataMovement) o;
        return name.equals(that.name) &&
                movement == that.movement;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, movement);
    }
}
