package org.cfp4j.springmvc.analyzer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

import java.util.Map;
import java.util.Set;

import org.cfp4j.entity.Movement;
import org.cfp4j.springmvc.Util;
import org.cfp4j.springmvc.model.DataMovement;
import org.junit.Test;

import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtMethod;

public class ModelAttributeMethodAnalyzerTest {

    private static CtClass<?> controller = Util.extractController("test.root.packages.controller.ModelAttributMethodController");
    private static Map<String, CtMethod<?>> appTypeMethodCache = Util.appTypeMethodCache;

    private ModelAttributeMethodAnalyzer analyzer = new ModelAttributeMethodAnalyzer("test.root.packages");

    @Test
    public void testModelAttributMethodThatLoadUser(){
    	String methodName = "findUser";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        
        Set<DataMovement> dataMovements = analyzer.extract(methodToTest, appTypeMethodCache);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(3);
        assertThat(dataMovements).extracting("name", "movement").contains(tuple("USERID", Movement.ENTRY), tuple("USER", Movement.READ), tuple("USER", Movement.EXIT));
    }
    
    @Test
    public void testModelAttributMethodThatLoadUserCollection(){
    	String methodName = "selectUsers";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        
        Set<DataMovement> dataMovements = analyzer.extract(methodToTest, appTypeMethodCache);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(2);
        assertThat(dataMovements).extracting("name", "movement").contains(tuple("USER_LIST", Movement.READ),tuple("USER_LIST", Movement.EXIT));
    }
    
    @Test
    public void testModelAttributMethodThatReturnsNewInstance(){
    	String methodName = "init";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        
        Set<DataMovement> dataMovements = analyzer.extract(methodToTest, appTypeMethodCache);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isEmpty();
    }
    
    @Test
    public void testModelAttributMethodThatPersistAddressInDB(){
    	String methodName = "saveAddress";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        
        Set<DataMovement> dataMovements = analyzer.extract(methodToTest, appTypeMethodCache);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(2);
        assertThat(dataMovements).extracting("name", "movement").contains(tuple("ADDRESS", Movement.ENTRY), tuple("ADDRESS", Movement.WRITE));
    }
}
