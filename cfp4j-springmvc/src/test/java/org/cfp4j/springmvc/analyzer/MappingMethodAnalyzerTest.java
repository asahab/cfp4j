package org.cfp4j.springmvc.analyzer;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collections;
import java.util.Map;

import org.cfp4j.entity.DataGroup;
import org.cfp4j.entity.FunctionalProcess;
import org.cfp4j.entity.Movement;
import org.cfp4j.springmvc.Util;
import org.junit.Test;

import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtMethod;


public class MappingMethodAnalyzerTest {

	private static CtClass<?> controller = Util.extractController("test.root.packages.controller.MappingMethodController");
    private static Map<String, CtMethod<?>> appTypeMethodCache = Util.appTypeMethodCache;
    private MappingMethodAnalyzer analyzer = new MappingMethodAnalyzer("test.root.packages");

    @Test
    public void testGetMappingMethodThatLoadUserBasedOnProvidedID() {
        String methodName = "getUser";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();

        FunctionalProcess functionalProcess = analyzer.analyze(methodToTest, appTypeMethodCache, Collections.emptySet());

        assertThat(functionalProcess).isNotNull();
        assertThat(functionalProcess.getName()).isEqualTo(methodName);
        assertThat(functionalProcess.getDataGroupSet()).isNotEmpty().hasSize(2);
        assertThat(functionalProcess.getDataGroupSet()).extracting(DataGroup::getName).containsOnly("USERID", "USER");
        assertThat(functionalProcess.getDataGroupSet()).flatExtracting(DataGroup::getMovements).containsOnly(Movement.ENTRY,Movement.READ,Movement.EXIT);
    }
    
    @Test
    public void testGetMappingMethodThatLoadUserBasedOnProvidedName() {
        String methodName = "getUserByName";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();

        FunctionalProcess functionalProcess = analyzer.analyze(methodToTest, appTypeMethodCache, Collections.emptySet());

        assertThat(functionalProcess).isNotNull();
        assertThat(functionalProcess.getName()).isEqualTo(methodName);
        assertThat(functionalProcess.getDataGroupSet()).isNotEmpty().hasSize(2);
        assertThat(functionalProcess.getDataGroupSet()).extracting(DataGroup::getName).containsOnly("NAME", "USER");
        assertThat(functionalProcess.getDataGroupSet()).flatExtracting(DataGroup::getMovements).containsOnly(Movement.ENTRY,Movement.READ,Movement.EXIT);
    }
    
    @Test
    public void testGetMappingMethodThatLoadAllUsers() {
        String methodName = "loadUsers";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();

        FunctionalProcess functionalProcess = analyzer.analyze(methodToTest, appTypeMethodCache, Collections.emptySet());

        assertThat(functionalProcess).isNotNull();
        assertThat(functionalProcess.getName()).isEqualTo(methodName);
        assertThat(functionalProcess.getDataGroupSet()).isNotEmpty().hasSize(2);
        assertThat(functionalProcess.getDataGroupSet()).extracting(DataGroup::getName).containsOnly("TRIGGER", "USER_LIST");
        assertThat(functionalProcess.getDataGroupSet()).flatExtracting(DataGroup::getMovements).containsOnly(Movement.ENTRY,Movement.READ,Movement.EXIT);
    }

    
    
    @Test
    public void testGetMappingMethodThatLoadAllUsersUsingModelAddAttributeWithTwoArguments() {
        String methodName = "loadUsersUsingModelAddAttributeWithTwoArguments";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();

        FunctionalProcess functionalProcess = analyzer.analyze(methodToTest, appTypeMethodCache, Collections.emptySet());

        assertThat(functionalProcess).isNotNull();
        assertThat(functionalProcess.getName()).isEqualTo(methodName);
        assertThat(functionalProcess.getDataGroupSet()).isNotEmpty().hasSize(2);
        assertThat(functionalProcess.getDataGroupSet()).extracting(DataGroup::getName).containsOnly("TRIGGER", "USER_LIST");
        assertThat(functionalProcess.getDataGroupSet()).flatExtracting(DataGroup::getMovements).containsOnly(Movement.ENTRY,Movement.READ,Movement.EXIT);
    }
    
    @Test
    public void testRequestMappingMethodThatLoadUserBasedOnProvidedID() {
        String methodName = "initUpdateUserForm";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();

        FunctionalProcess functionalProcess = analyzer.analyze(methodToTest, appTypeMethodCache, Collections.emptySet());

        assertThat(functionalProcess).isNotNull();
        assertThat(functionalProcess.getName()).isEqualTo(methodName);
        assertThat(functionalProcess.getDataGroupSet()).isNotEmpty().hasSize(2);
        assertThat(functionalProcess.getDataGroupSet()).extracting(DataGroup::getName).containsOnly("USERID", "USER");
        assertThat(functionalProcess.getDataGroupSet()).flatExtracting(DataGroup::getMovements).containsOnly(Movement.ENTRY,Movement.READ,Movement.EXIT);
    }
    
    @Test
    public void testRequestMappingMethodThatUpdateUser() {
        String methodName = "updateUserFormWithoutService";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();

        FunctionalProcess functionalProcess = analyzer.analyze(methodToTest, appTypeMethodCache, Collections.emptySet());

        assertThat(functionalProcess).isNotNull();
        assertThat(functionalProcess.getName()).isEqualTo(methodName);
        assertThat(functionalProcess.getDataGroupSet()).isNotEmpty().hasSize(2);
        assertThat(functionalProcess.getDataGroupSet()).extracting(DataGroup::getName).containsOnly("MESSAGE", "USER");
        assertThat(functionalProcess.getDataGroupSet()).filteredOn(dg->dg.getName().equals("MESSAGE")).flatExtracting(DataGroup::getMovements).containsOnly(Movement.EXIT);
        assertThat(functionalProcess.getDataGroupSet()).filteredOn(dg->dg.getName().equals("USER")).flatExtracting(DataGroup::getMovements).containsOnly(Movement.ENTRY, Movement.WRITE);
    }
    
    @Test
    public void testRequestMappingMethodThatUpdateUserAndPassword() {
        String methodName = "updateUserForm";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();

        FunctionalProcess functionalProcess = analyzer.analyze(methodToTest, appTypeMethodCache, Collections.emptySet());

        assertThat(functionalProcess).isNotNull();
        assertThat(functionalProcess.getName()).isEqualTo(methodName);
        assertThat(functionalProcess.getDataGroupSet()).isNotEmpty().hasSize(3);
        assertThat(functionalProcess.getDataGroupSet()).extracting(DataGroup::getName).containsOnly("MESSAGE", "USER","ENCRYPTEDPASSWORD");
        assertThat(functionalProcess.getDataGroupSet()).filteredOn(dg->dg.getName().equals("MESSAGE")).flatExtracting(DataGroup::getMovements).containsOnly(Movement.EXIT);
        assertThat(functionalProcess.getDataGroupSet()).filteredOn(dg->dg.getName().equals("USER")).flatExtracting(DataGroup::getMovements).containsOnly(Movement.ENTRY, Movement.WRITE);
        assertThat(functionalProcess.getDataGroupSet()).filteredOn(dg->dg.getName().equals("ENCRYPTEDPASSWORD")).flatExtracting(DataGroup::getMovements).containsOnly(Movement.WRITE);
    }
    
    @Test
    public void testPostMappingMethodThatUpdateUserAndPassword() {
        String methodName = "updateUser";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();

        FunctionalProcess functionalProcess = analyzer.analyze(methodToTest, appTypeMethodCache, Collections.emptySet());

        assertThat(functionalProcess).isNotNull();
        assertThat(functionalProcess.getName()).isEqualTo(methodName);
        assertThat(functionalProcess.getDataGroupSet()).isNotEmpty().hasSize(3);
        assertThat(functionalProcess.getDataGroupSet()).extracting(DataGroup::getName).containsOnly("MESSAGE", "USER","ENCRYPTEDPASSWORD");
        assertThat(functionalProcess.getDataGroupSet()).flatExtracting(DataGroup::getMovements).containsOnly(Movement.ENTRY,Movement.WRITE,Movement.EXIT);
    }
}