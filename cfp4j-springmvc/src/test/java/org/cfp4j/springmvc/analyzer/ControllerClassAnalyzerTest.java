package org.cfp4j.springmvc.analyzer;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Map;

import org.cfp4j.entity.DataGroup;
import org.cfp4j.entity.FunctionalProcess;
import org.cfp4j.entity.Movement;
import org.cfp4j.entity.Reference;
import org.cfp4j.springmvc.Util;
import org.junit.Test;

import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtMethod;

public class ControllerClassAnalyzerTest {
	private static Map<String, CtMethod<?>> appTypeMethodCache = Util.appTypeMethodCache;
    private ControllerClassAnalyzer analyzer = new ControllerClassAnalyzer("test.root.packages");

    @Test
    public void testControllerAnalysisWithModelAttributeMethod() {
    	CtClass<?> controller = Util.extractController("test.root.packages.controller.AddressController");
        Reference reference = analyzer.analyze(controller, appTypeMethodCache);

        assertThat(reference).isNotNull();
        assertThat(reference.getName()).isEqualTo("AddressController");
        assertThat(reference.functionalPoints()).isEqualTo(6);
        assertThat(reference.getFunctionalProcesses()).isNotNull().hasSize(1);
        FunctionalProcess functionalProcess = reference.getFunctionalProcesses().iterator().next();
        assertThat(functionalProcess).isNotNull();
        assertThat(functionalProcess.getName()).isEqualTo("processCreationForm");
        assertThat(functionalProcess.getDataGroupSet()).isNotEmpty().hasSize(4);
        assertThat(functionalProcess.getDataGroupSet()).extracting(DataGroup::getName).containsOnly("MESSAGE","USERID", "USER", "ADDRESS");
        assertThat(functionalProcess.getDataGroupSet()).filteredOn(dg->dg.getName().equals("MESSAGE")).flatExtracting(DataGroup::getMovements).containsOnly(Movement.EXIT);
        assertThat(functionalProcess.getDataGroupSet()).filteredOn(dg->dg.getName().equals("USERID")).flatExtracting(DataGroup::getMovements).containsOnly(Movement.ENTRY);
        assertThat(functionalProcess.getDataGroupSet()).filteredOn(dg->dg.getName().equals("USER")).flatExtracting(DataGroup::getMovements).containsOnly(Movement.READ, Movement.EXIT);
        assertThat(functionalProcess.getDataGroupSet()).filteredOn(dg->dg.getName().equals("ADDRESS")).flatExtracting(DataGroup::getMovements).containsOnly(Movement.ENTRY, Movement.WRITE);
        
    }
    
    @Test
    public void testControllerAnalysisWithMultiFunctionalProcesses() {
    	CtClass<?> controller = Util.extractController("test.root.packages.controller.UserController");
        Reference reference = analyzer.analyze(controller, appTypeMethodCache);

        assertThat(reference).isNotNull();
        assertThat(reference.getName()).isEqualTo("UserController");
        assertThat(reference.functionalPoints()).isEqualTo(16);
        assertThat(reference.getFunctionalProcesses()).isNotNull().hasSize(5);
        assertThat(reference.getFunctionalProcesses()).filteredOn(fp->fp.getName().equals("getUser")).first().extracting(FunctionalProcess::functionalPoints).isEqualTo(3);
        assertThat(reference.getFunctionalProcesses()).filteredOn(fp->fp.getName().equals("updateUserForm")).first().extracting(FunctionalProcess::functionalPoints).isEqualTo(4);
        assertThat(reference.getFunctionalProcesses()).filteredOn(fp->fp.getName().equals("loadUsers")).first().extracting(FunctionalProcess::functionalPoints).isEqualTo(3);
        assertThat(reference.getFunctionalProcesses()).filteredOn(fp->fp.getName().equals("initUpdateUserForm")).first().extracting(FunctionalProcess::functionalPoints).isEqualTo(3);
        assertThat(reference.getFunctionalProcesses()).filteredOn(fp->fp.getName().equals("updateUserFormWithoutService")).first().extracting(FunctionalProcess::functionalPoints).isEqualTo(3);
        
    }

}
