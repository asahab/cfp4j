package org.cfp4j.springmvc.extractor;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

import java.util.Set;

import org.cfp4j.entity.Movement;
import org.cfp4j.springmvc.Util;
import org.cfp4j.springmvc.model.DataMovement;
import org.junit.Test;

import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtMethod;

public class MappingMethodExitMovementExtractorTest {
   
    private static CtClass<?> controller = Util.extractController("test.root.packages.controller.ExitMappingController");
	
    private MappingMethodExitMovementExtractor extractor = new MappingMethodExitMovementExtractor("test.root.packages");

    //util.Map
    @Test
    public void testNoExitDataGroupAfterAddingAttributeToModelMapFromLocalInstance() {
        String methodName = "addAttributeFromLocalInstance";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);
        assertThat(dataMovements).isNotNull().isEmpty();
    }

    @Test
    public void testExitDataGroupAfterAddingAttributeToModelMapFromLocalVariableWithEntity() {
        String methodName = "addAttributeToMapModelFromLocalVariable";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);
        
        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER", Movement.EXIT));
    }

    @Test
    public void testNoExitDataGroupAfterAddingAttributeToModelMapFromService() {
        String methodName = "addAttributeToMapModelFromService";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);
        
        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER", Movement.EXIT));
    }

    @Test
    public void testExitDataGroupAfterAddingAttributeToModelMapFromRepository() {
        String methodName = "addAttributeToMapModelFromRepository";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER", Movement.EXIT));
    }

    // ui.Model
    @Test
    public void testNoExitDataGroupAfterAddingAttributeToModelFromLocalInstance() {
        String methodName = "addAttributeToModelFromLocalInstance";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);
        
        assertThat(dataMovements).isNotNull().isEmpty();
    }

    @Test
    public void testExitDataGroupAfterAddingAttributeToModelFromLocalVariableWithEntity() {
        String methodName = "addAttributeToModelFromLocalVariable";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER", Movement.EXIT));
    }
    
    
    
    @Test
    public void testExitDataGroupAfterAddingAttributeWithTwoVariablesToModel() {
        String methodName = "addAttributeToModelWithTwoVariables";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER", Movement.EXIT));
    }

    @Test
    public void testNoExitDataGroupAfterAddingAttributeToModelFromService() {
        String methodName = "addAttributeToModelFromService";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER", Movement.EXIT));
    }

    @Test
    public void testExitDataGroupAfterAddingAttributeToModelFromRepository() {
        String methodName = "addAttributeToModelFromRepository";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER", Movement.EXIT));
    }
    // ui.ModelMap
    @Test
    public void testNoExitDataGroupAfterAddingAttributeToUiModelMapFromLocalInstance() {
        String methodName = "addAttributeToUiMapModelFromLocalInstance";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);
        assertThat(dataMovements).isNotNull().isEmpty();
    }

    @Test
    public void testExitDataGroupAfterAddingAttributeToUiModelMapFromLocalVariableWithEntity() {
        String methodName = "addAttributeToUiMapModelFromLocalVariable";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);
        
        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER", Movement.EXIT));
    }

    @Test
    public void testNoExitDataGroupAfterAddingAttributeToUiModelMapFromService() {
        String methodName = "addAttributeToUiMapModelFromService";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);
        
        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER", Movement.EXIT));
    }

    @Test
    public void testExitDataGroupAfterAddingAttributeToUiModelMapFromRepository() {
        String methodName = "addAttributeToUiMapModelFromRepository";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);
        
        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER", Movement.EXIT));
    }

    //ModelAndView
    @Test
    public void testNoExitDataGroupAfterAddingAttributeToModelAndViewFromLocalInstance() {
        String methodName = "addAttributeToModelAndViewFromLocalInstance";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);
        assertThat(dataMovements).isNotNull().isEmpty();
    }

    @Test
    public void testExitDataGroupAfterAddingAttributeToModelAndViewFromLocalVariableWithEntity() {
        String methodName = "addAttributeToModelAndViewFromLocalVariable";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);
        
        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER", Movement.EXIT));
    }

    @Test
    public void testNoExitDataGroupAfterAddingAttributeToModelAndViewFromService() {
        String methodName = "addAttributeToModelAndViewFromService";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);
        
        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER", Movement.EXIT));
    }

    @Test
    public void testExitDataGroupAfterAddingAttributeToModelAndViewFromRepository() {
        String methodName = "addAttributeToModelAndViewFromRepository";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);
        
        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER", Movement.EXIT));
    }
    
    @Test
    public void testExitDataGroupForMethodThatHasResponseBodyAsAnnotationAndReturnInvocationToString() {
        String methodName = "userExists";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);
        
        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("toString[userExists]", Movement.EXIT));
    }
    
    @Test
    public void testExitDataGroupForMethodThatHasResponseBodyAsAnnotationAndReturnEntityGetString() {
        String methodName = "userGetName";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);
        
        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("getName", Movement.EXIT));
    }
    
    @Test
    public void testExitDataGroupForMethodThatHasResponseBodyAsAnnotationAndReturnString() {
        String methodName = "userLookupName";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);
        
        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("name", Movement.EXIT));
    }
    
    @Test
    public void testExitDataGroupForMethodThatHasResponseBodyAsAnnotationAndReturnEntity() {
        String methodName = "userLookup";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);
        
        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER", Movement.EXIT));
    }
    
    @Test
    public void testExitDataGroupForMethodThatHasResponseBodyAsAnnotationAndReturnEntityList() {
        String methodName = "usersLookup";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);
        
        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER_LIST", Movement.EXIT));
    }
}
