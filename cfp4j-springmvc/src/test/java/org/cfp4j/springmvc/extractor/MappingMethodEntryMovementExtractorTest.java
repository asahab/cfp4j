package org.cfp4j.springmvc.extractor;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;

import org.cfp4j.entity.Movement;
import org.cfp4j.springmvc.Util;
import org.cfp4j.springmvc.model.DataMovement;
import org.junit.Test;

import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtMethod;

public class MappingMethodEntryMovementExtractorTest {

	private static CtClass<?> controller = Util.extractController("test.root.packages.controller.EntryMappingController");

    private MappingMethodEntryMovementExtractor extractor = new MappingMethodEntryMovementExtractor("test.root.packages");

    @Test
    public void testExtractEntryMovementsWithOneMethodPrimitiveEntryParam() {
        String methodName = "getUser";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting(DataMovement::getName).containsOnly("USERID");
        assertThat(dataMovements).extracting(DataMovement::getMovement).containsOnly(Movement.ENTRY);
    }

    @Test
    public void testExtractEntryMovementsWithEntityEntryParamAndItsIdentifiantAsMethodPrimitiveEntryParam() {
        String methodName = "updateUserForm";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting(DataMovement::getName).containsOnly("USER");
        assertThat(dataMovements).extracting(DataMovement::getMovement).containsOnly(Movement.ENTRY);
    }

    @Test
    public void testExtractEntryMovementsWithTwoMethodEntityEntryParams() {
        String methodName = "updateUserAndAddress";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(2);
        assertThat(dataMovements).extracting(DataMovement::getName).containsOnly("USER", "ADDRESS");
        assertThat(dataMovements).extracting(DataMovement::getMovement).containsOnly(Movement.ENTRY);
    }

    @Test
    public void testExtractEntryMovementsWithOneMethodPrimitiveEntryParamAndModel() {
        String methodName = "initUpdateUserForm";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();
        Set<DataMovement> dataMovements = extractor.extract(methodToTest);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting(DataMovement::getName).containsOnly("USERID");
        assertThat(dataMovements).extracting(DataMovement::getMovement).containsOnly(Movement.ENTRY);
    }
}