package org.cfp4j.springmvc.extractor;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

import java.util.Map;
import java.util.Set;

import org.cfp4j.entity.Movement;
import org.cfp4j.springmvc.Util;
import org.cfp4j.springmvc.extractor.SpringDataReadWriteMovementExtractor;
import org.cfp4j.springmvc.model.DataMovement;
import org.junit.Test;

import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtMethod;

public class SpringDataReadWriteMovementExtractorTest {

	private static CtClass<?> controller = Util.extractController("test.root.packages.controller.ReadWriteMappingController");
	private static Map<String, CtMethod<?>> appTypeMethodCache = Util.appTypeMethodCache;
	
	private SpringDataReadWriteMovementExtractor extractor = new SpringDataReadWriteMovementExtractor();

    @Test
    public void testReadDataGroupFromService() {
        String methodName = "getUserFromService";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();

        Set<DataMovement> dataMovements = extractor.extract(appTypeMethodCache, methodToTest);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER", Movement.READ));
    }

    @Test
    public void testReadDataGroupFromServiceWithLocalVariableSave() {
        String methodName = "getUserFromServiceWithLocalVariableSave";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();

        Set<DataMovement> dataMovements = extractor.extract(appTypeMethodCache, methodToTest);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER", Movement.READ));
    }

    @Test
    public void testReadDataGroupFromRepository() {
        String methodName = "getUserFromRepository";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();

        Set<DataMovement> dataMovements = extractor.extract(appTypeMethodCache, methodToTest);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER", Movement.READ));
    }

    @Test
    public void testReadDataGroupFromRepositoryWithLocalVariableSave() {
        String methodName = "getUserFromRepositoryWithLocalVariableSave";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();

        Set<DataMovement> dataMovements = extractor.extract(appTypeMethodCache, methodToTest);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER", Movement.READ));
    }

    @Test
    public void testWriteDataGroupFromService() {
        String methodName = "saveUserViaServiceWithPasswordUpdateCustomMethod";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();

        Set<DataMovement> dataMovements = extractor.extract(appTypeMethodCache, methodToTest);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(2);
        assertThat(dataMovements).extracting("name", "movement").contains(tuple("USER", Movement.WRITE), tuple("ENCRYPTEDPASSWORD", Movement.WRITE));
    }

    @Test
    public void testWriteDataGroupFromRepository() {
        String methodName = "saveUserViaRepository";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();

        Set<DataMovement> dataMovements = extractor.extract(appTypeMethodCache, methodToTest);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER", Movement.WRITE));
    }

    @Test
    public void testReadDataGroupCollectionFromService() {
        String methodName = "loadUsersFromService";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();

        Set<DataMovement> dataMovements = extractor.extract(appTypeMethodCache, methodToTest);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER_LIST", Movement.READ));
    }

    @Test
    public void testReadDataGroupCollectionFromServiceWithLocalVariableSave() {
        String methodName = "loadUsersFromServiceWithLocalVariableSave";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();

        Set<DataMovement> dataMovements = extractor.extract(appTypeMethodCache, methodToTest);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER_LIST", Movement.READ));
    }

    @Test
    public void testReadDataGroupCollectionFromRepository() {
        String methodName = "loadUsersFromRepository";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();

        Set<DataMovement> dataMovements = extractor.extract(appTypeMethodCache, methodToTest);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER_LIST", Movement.READ));
    }

    @Test
    public void testReadDataGroupCollectionFromRepositoryWithLocalVariableSave() {
        String methodName = "loadUsersFromRepositoryWithLocalVariableSave";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();

        Set<DataMovement> dataMovements = extractor.extract(appTypeMethodCache, methodToTest);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1).contains(new DataMovement("USER_LIST", Movement.READ));
    }
    
    @Test
    public void testReadDataGroupFromRepositoryCustomMethod() {
        String methodName = "loadUserFromRepositoryWithCustomMethod";
        CtMethod<?> methodToTest = controller.getAllMethods().stream().filter(m -> m.getSimpleName().equalsIgnoreCase(methodName)).findFirst().get();

        Set<DataMovement> dataMovements = extractor.extract(appTypeMethodCache, methodToTest);

        assertThat(dataMovements).isNotNull();
        assertThat(dataMovements).isNotEmpty().hasSize(1);
        assertThat(dataMovements).extracting("name", "movement").containsExactly(tuple("USER", Movement.READ));
    }
}
