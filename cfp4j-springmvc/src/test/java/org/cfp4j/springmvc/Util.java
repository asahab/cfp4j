package org.cfp4j.springmvc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import spoon.Launcher;
import spoon.SpoonAPI;
import spoon.reflect.CtModel;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtMethod;

public class Util {
	
	private static CtModel model = buildModel();

    public static Map<String, CtMethod<?>> appTypeMethodCache = new HashMap<>();
    static {
        model.getAllTypes().stream().filter(t -> t.getQualifiedName().startsWith("test.root.packages"))
                .forEach(t-> t.getAllMethods()
                        .forEach(m-> appTypeMethodCache.put(String.format("%s.%s",t.getQualifiedName(),m.getSignature()), m)));
    }
    
    

    public static CtClass<?> extractController(String controllerQualifiedName) {
        List<CtClass<?>> controllers = model.getElements(e -> e.getAnnotations().stream().filter(a -> a.getType().getQualifiedName().equals("org.springframework.stereotype.Controller")).findFirst().isPresent());
        return controllers.stream().filter(c->c.getQualifiedName().equals(controllerQualifiedName)).findFirst().get();

    }
    
    private static CtModel buildModel() {
        String resourceToTestPath = Util.class.getResource("source-code/test/root/packages").getFile();
        SpoonAPI spoon = new Launcher();
        spoon.addInputResource(resourceToTestPath);
        return spoon.buildModel();

    }

}
