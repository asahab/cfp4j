package org.cfp4j.springmvc;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;

import org.cfp4j.ICfpCalculator;
import org.cfp4j.entity.Reference;
import org.junit.Test;

public class SpringMvcCfpCalculatorTest {

	
	private ICfpCalculator calculator = new SpringMvcCfpCalculator("test.root.packages");
	
	@Test
    public void testSpringMvcCfpCalculator() {
		Set<Reference> references = calculator.calculate("src/test/resources/org/cfp4j/springmvc/calculator/test/root/packages");
		assertThat(references).isNotNull().hasSize(2);
		assertThat(references).filteredOn(ref->ref.getName().equals("AddressController")).first().extracting(Reference::functionalPoints).isEqualTo(6);
		assertThat(references).filteredOn(ref->ref.getName().equals("UserController")).first().extracting(Reference::functionalPoints).isEqualTo(13);
	}
}
