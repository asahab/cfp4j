package test.root.packages.controller;

import java.util.Collection;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import test.root.packages.entity.User;
import test.root.packages.repository.UserRepository;
import test.root.packages.service.UserService;

@Controller
public class ReadWriteMappingController {

	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserRepository userRepository;

	@GetMapping("/users/{userId}")
	public ModelAndView getUserFromService(@PathVariable("userId") int userId) {
		ModelAndView mav = new ModelAndView("users/UserDetails");
		mav.addObject(this.userService.loadUser(userId));
		return mav;
	}
	
	@GetMapping("/users/{userId}")
	public ModelAndView getUserFromServiceWithLocalVariableSave(@PathVariable("userId") int userId) {
		User user = this.userService.loadUser(userId);
		ModelAndView mav = new ModelAndView("users/UserDetails");
		mav.addObject(user);
		return mav;
	}
	
	@GetMapping("/users/{userId}")
	public ModelAndView getUserFromRepository(@PathVariable("userId") int userId) {
		ModelAndView mav = new ModelAndView("users/UserDetails");
		mav.addObject(this.userRepository.findById(userId));
		return mav;
	}
	
	@GetMapping("/users/{userId}")
	public ModelAndView getUserFromRepositoryWithLocalVariableSave(@PathVariable("userId") int userId) {
		User user = this.userRepository.getOne(userId);
		ModelAndView mav = new ModelAndView("users/UserDetails");
		mav.addObject(user);
		return mav;
	}

	@PostMapping("/users/{userId}/edit")
	public String saveUserViaServiceWithPasswordUpdateCustomMethod(@Valid User user, BindingResult result, @PathVariable("userId") int userId) {
		user.setId(userId);
		this.userService.saveUser(user);
		return "redirect:/users/{userId}";
	}
	
	@PostMapping("/users/{userId}/edit")
	public String saveUserViaRepository(@Valid User user, BindingResult result, @PathVariable("userId") int userId) {
		user.setId(userId);
		this.userRepository.save(user);
		return "redirect:/users/{userId}";
	}

	@GetMapping("/users")
	public String loadUsersFromService(Map<String, Object> model) {

		model.put("selections", this.userService.loadUsers());
		return "owners/ownersList";
	}
	
	@GetMapping("/users")
	public String loadUsersFromServiceWithLocalVariableSave(Map<String, Object> model) {

		Collection<User> results = this.userService.loadUsers();
		model.put("selections", results);
		return "owners/ownersList";
	}
	
	@GetMapping("/users")
	public String loadUsersFromRepository(Map<String, Object> model) {
		model.put("selections", this.userRepository.findUsers());
		return "owners/ownersList";
	}
	
	@GetMapping("/users")
	public String loadUsersFromRepositoryWithLocalVariableSave(Map<String, Object> model) {
		Collection<User> results = this.userRepository.findUsers();
		model.put("selections", results);
		return "owners/ownersList";
	}

	@GetMapping("/users")
	public ModelAndView loadUserFromRepositoryWithCustomMethod(@RequestParam("name") String name) {
		ModelAndView mav = new ModelAndView("users/UserDetails");
		mav.addObject(this.userService.loadUserByName(name));
		return mav;
	}
}
