package test.root.packages.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import test.root.packages.entity.Address;
import test.root.packages.entity.User;
import test.root.packages.service.UserService;

@Controller
public class EntryMappingController {
    @Autowired
    private UserService userService;

    @GetMapping("/users/{userId}")
    public ModelAndView getUser(@PathVariable("userId") int userId) {
        return new ModelAndView("users/UserDetails");
    }

    @PostMapping("/users/{userId}/edit")
    public String updateUserForm(@Valid User user, BindingResult result, @PathVariable("userId") int userId) {
        user.setId(userId);
        return "redirect:/users/{userId}";
    }

    @PostMapping("/users/{userId}/edit")
    public String updateUserAndAddress(@Valid User user, @Valid Address addess) {
        return "redirect:/users/{userId}";
    }

    @GetMapping("/users/{userId}/edit")
    public String initUpdateUserForm(@PathVariable("userId") int userId, Model model) {
        User user = this.userService.loadUser(userId);
        model.addAttribute(user);
        return "view";
    }
}
