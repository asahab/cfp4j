package test.root.packages.controller;

import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import test.root.packages.entity.User;
import test.root.packages.repository.UserRepository;
import test.root.packages.service.UserService;

@Controller
public class ExitMappingController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/entities/new")
    public String addAttributeFromLocalInstance(Map<String, Object> model) {
        User user = new User();
        model.put("user", user);
        return "display_view";
    }

    @GetMapping("/users/{userId}/edit")
    public String addAttributeToMapModelFromLocalVariable(@PathVariable("userId") int userId, Map<String, Object> model) {
        User user = this.userService.loadUser(userId);
        model.put("user",user);
        return "view";
    }

    @GetMapping("/users/{userId}/edit")
    public String addAttributeToMapModelFromService(@PathVariable("userId") int userId, Map<String, Object> model) {
        model.put("user", this.userService.loadUser(userId));
        return "view";
    }

    @GetMapping("/users/{userId}/edit")
    public String addAttributeToMapModelFromRepository(@PathVariable("userId") int userId, Map<String, Object> model) {
        model.put("user", this.userRepository.findById(userId));
        return "view";
    }

    @GetMapping("/entities/new")
    public String addAttributeToModelFromLocalInstance(Model model) {
        User user = new User();
        model.addAttribute(user);
        return "display_view";
    }

    @GetMapping("/users/{userId}/edit")
    public String addAttributeToModelFromLocalVariable(@PathVariable("userId") int userId, Model model) {
        User user = this.userService.loadUser(userId);
        model.addAttribute(user);
        return "view";
    }
    
    @GetMapping("/users/{userId}/edit")
    public String addAttributeToModelWithTwoVariables(Model model) {
        model.addAttribute("user", userService.loadUser(userId));
        return "view";
    }

    @GetMapping("/users/{userId}/edit")
    public String addAttributeToModelFromService(@PathVariable("userId") int userId, Model model) {
        model.addAttribute(this.userService.loadUser(userId));
        return "view";
    }

    @GetMapping("/users/{userId}/edit")
    public String addAttributeToModelFromRepository(@PathVariable("userId") int userId, Model model) {
        model.addAttribute(this.userRepository.findById(userId));
        return "view";
    }

    @GetMapping("/entities/new")
    public String addAttributeToUiMapModelFromLocalInstance(ModelMap model) {
        User user = new User();
        model.addAttribute(user);
        return "display_view";
    }

    @GetMapping("/users/{userId}/edit")
    public String addAttributeToUiMapModelFromLocalVariable(@PathVariable("userId") int userId, ModelMap model) {
        User user = this.userService.loadUser(userId);
        model.addAttribute(user);
        return "view";
    }

    @GetMapping("/users/{userId}/edit")
    public String addAttributeToUiMapModelFromService(@PathVariable("userId") int userId, ModelMap model) {
        model.put("user", this.userService.loadUser(userId));
        return "view";
    }

    @GetMapping("/users/{userId}/edit")
    public String addAttributeToUiMapModelFromRepository(@PathVariable("userId") int userId, ModelMap model) {
        model.addAttribute(this.userRepository.findById(userId));
        return "view";
    }

    @GetMapping("/entities/new")
    public ModelAndView addAttributeToModelAndViewFromLocalInstance() {
        User user = new User();
        ModelAndView mav = new ModelAndView("users/UserDetails");
        mav.addObject(user);
        return mav;
    }

    @GetMapping("/users/{userId}/edit")
    public ModelAndView addAttributeToModelAndViewFromLocalVariable(@PathVariable("userId") int userId) {
        User user = this.userService.loadUser(userId);
        ModelAndView mav = new ModelAndView("users/UserDetails");
        mav.addObject(user);
        return mav;
    }

    @GetMapping("/users/{userId}/edit")
    public ModelAndView addAttributeToModelAndViewFromService(@PathVariable("userId") int userId) {
        ModelAndView mav = new ModelAndView("users/UserDetails");
        mav.addObject(this.userService.loadUser(userId));
        return mav;
    }

    @GetMapping("/users/{userId}/edit")
    public ModelAndView addAttributeToModelAndViewFromRepository(@PathVariable("userId") int userId) {
        ModelAndView mav = new ModelAndView("users/UserDetails");
        mav.addObject(this.userRepository.findById(userId));
        return mav;
    }
    
    @RequestMapping("/users/{userId}")
    @ResponseBody
    public String userExists(@PathVariable("userId") int userId) {
        User user = this.userService.loadUser(userId);
        boolean userExists = user != null;
        return Boolean.toString(userExists);
    }
    
    @RequestMapping("/users/{userId}")
    @ResponseBody
    public String userGetName(@PathVariable("userId") int userId) {
        User user = this.userService.loadUser(userId);
        return user.getName();
    }
    
    @RequestMapping("/users/{userId}")
    @ResponseBody
    public String userLookupName(@PathVariable("userId") int userId) {
        User user = this.userService.loadUser(userId);
        String name = user.getName();
        return name;
    }
    
    @RequestMapping("/users/{userId}")
    @ResponseBody
    public User userLookup(@PathVariable("userId") int userId) {
        User user = this.userService.loadUser(userId);
        return user;
    }
    
    @RequestMapping("/users")
    @ResponseBody
    public Collection<User> usersLookup() {
        return this.userService.loadUsers();
    }
}
