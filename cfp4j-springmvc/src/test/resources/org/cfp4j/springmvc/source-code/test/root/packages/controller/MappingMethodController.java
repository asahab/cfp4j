package test.root.packages.controller;

import java.util.Collection;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import test.root.packages.entity.Address;
import test.root.packages.entity.User;
import test.root.packages.repository.UserRepository;
import test.root.packages.service.AddressService;
import test.root.packages.service.UserService;

@Controller
public class MappingMethodController {

	@Autowired
	private UserService userService;
	
	@Autowired
    private UserRepository userRepository;

	@GetMapping("/users/{userId}")
	public ModelAndView getUser(@PathVariable("userId") int userId) {
		ModelAndView mav = new ModelAndView("users/UserDetails");
		mav.addObject(this.userService.loadUser(userId));
		return mav;
	}

	@GetMapping("/users/{userId}")
	public ModelAndView getUserByName(@PathVariable("userId") String name) {
		ModelAndView mav = new ModelAndView("users/UserDetails");
		mav.addObject(this.userService.loadUserByName(name));
		return mav;
	}
	@RequestMapping("/users/{userId}/edit")
	public String updateUserFormWithoutService(@Valid User user, BindingResult result, @PathVariable("userId") int userId) {
		if (result.hasErrors()) {
			return "VIEWS_OWNER_CREATE_OR_UPDATE_FORM";
		} else {
			user.setId(userId);
			this.userRepository.save(user);
			return "redirect:/users/{userId}";
		}
	}
	
	@RequestMapping("/users/{userId}/edit")
	public String updateUserForm(@Valid User user, BindingResult result, @PathVariable("userId") int userId) {
		if (result.hasErrors()) {
			return "VIEWS_OWNER_CREATE_OR_UPDATE_FORM";
		} else {
			user.setId(userId);
			this.userService.saveUser(user);
			return "redirect:/users/{userId}";
		}
	}
	
	@PostMapping("/users/{userId}/edit")
	public String updateUser(@Valid User user, BindingResult result, @PathVariable("userId") int userId) {
		if (result.hasErrors()) {
			return "VIEWS_OWNER_CREATE_OR_UPDATE_FORM";
		} else {
			user.setId(userId);
			this.userService.saveUser(user);
			return "redirect:/users/{userId}";
		}
	}

	@GetMapping("/users")
	public String loadUsers(Map<String, Object> model) {
		// find owners by last name
		Collection<User> results = this.userService.loadUsers();
		// multiple owners found
		model.put("selections", results);
		return "owners/ownersList";
	}

	@RequestMapping("/users/{userId}/edit")
	public String initUpdateUserForm(@PathVariable("userId") int userId, Model model) {
		User user = this.userService.loadUser(userId);
		model.addAttribute(user);
		return "view";
	}
	
	@RequestMapping("/userList")
	public String loadUsersUsingModelAddAttributeWithTwoArguments(Model model) {
		model.addAttribute("users", userService.loadUsers());
		return "view";
	}
}
