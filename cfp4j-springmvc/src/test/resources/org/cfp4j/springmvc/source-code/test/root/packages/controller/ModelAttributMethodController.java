package test.root.packages.controller;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import test.root.packages.entity.Address;
import test.root.packages.entity.User;
import test.root.packages.service.AddressService;
import test.root.packages.service.UserService;

@Controller
@RequestMapping("/users/{userId}")
public class ModelAttributMethodController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private AddressService addressService;
	
	@ModelAttribute("user")
    public User init() {
        return new User();
    }
	
	@ModelAttribute("user")
    public User findUser(@PathVariable("userId") int userId) {
        return this.userService.loadUser(userId);
    }
	
	@ModelAttribute("users")
    public Collection<User> selectUsers() {
        return this.userService.loadUsers();
    }
	
	@ModelAttribute
	public void saveAddress(@Valid Address address, BindingResult result, @PathVariable("addressId") int addressId) {
		address.setId(addressId);
		this.addressService.saveAddress(address);
	}
	
	
    @PostMapping("/addresses/new")
    public String processCreationForm(User user, @Valid Address address, BindingResult result, ModelMap model) {
        user.setAddress(address);
        this.addressService.saveAddress(address);
        return "view";
    }
}
