package test.root.packages.repository;

import java.util.Collection;

import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import test.root.packages.entity.Address;

public interface AddressRepository extends Repository<Address, Integer>{

	
	Address findById(@Param("id") Integer id);
	
	void save(Address address);
	
	Collection<Address> findAddresses();
}
