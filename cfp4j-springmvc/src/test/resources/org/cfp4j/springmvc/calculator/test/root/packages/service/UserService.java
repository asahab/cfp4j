package test.root.packages.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import test.root.packages.entity.User;
import test.root.packages.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	
	public User loadUser(Integer id) {
		return userRepository.getOne(id);
	}
	
	public void saveUser(User user) {
		userRepository.save(user);
		String encryptedPassword = "test0000";
		userRepository.updateEncryptedPassword(encryptedPassword);
	}
	
	public Collection<User> loadUsers() {
		return userRepository.findUsers();
	}
	
	public User loadUserByName(String name) {
		return userRepository.findByName(name);
	}

}
