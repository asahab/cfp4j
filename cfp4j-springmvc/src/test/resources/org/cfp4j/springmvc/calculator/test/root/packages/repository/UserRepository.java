package test.root.packages.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import test.root.packages.entity.User;

public interface UserRepository extends JpaRepository<User, Integer>{

	Collection<User> findUsers();
	
	User findByName(String name);
	
	void updateEncryptedPassword(String encryptedPassword);
}
