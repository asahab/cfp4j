package test.root.packages.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import test.root.packages.entity.Address;
import test.root.packages.repository.AddressRepository;

@Service
public class AddressService {
	
	@Autowired
	private AddressRepository addressRepository;
	
	
	public Address loadAddress(Integer id) {
		return addressRepository.findById(id);
	}
	
	public void saveAddress(Address address) {
		addressRepository.save(address);
	}
	
	public Collection<Address> loadAddresses() {
		return addressRepository.findAddresses();
	}

}
