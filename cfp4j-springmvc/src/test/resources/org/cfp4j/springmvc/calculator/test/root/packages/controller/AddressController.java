package test.root.packages.controller;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import test.root.packages.entity.Address;
import test.root.packages.entity.User;
import test.root.packages.service.AddressService;
import test.root.packages.service.UserService;

@Controller
public class AddressController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private AddressService addressService;
	
	@ModelAttribute("user")
    public User findUser(@PathVariable("userId") int userId) {
        return this.userService.loadUser(userId);
    }
	
    @PostMapping("/users/{userId}/addresses/new")
    public String processCreationForm(User user, @Valid Address address, BindingResult result, ModelMap model) {
        user.setAddress(address);
        this.addressService.saveAddress(address);
        return "view";
    }
}
