package test.root.packages.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
class CrashController {

    @GetMapping("/help")
    public String exception() {
        throw new RuntimeException("Unexpected exception");
    }

}