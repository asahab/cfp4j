# CFP4J - Spring MVC

This module contains the implementation of the COSMIC function point measurement for Java business web applications using the SpringMVC and Spring Data frameworks. The expected Spring framework version is 5.x.x and above.

The implementation aims to identify data movements (Entry, Exit, Read, Write) from the analysis of the source code of a _Web Java_ application using the _Spring MVC_ framework. 

Below are the details of the defined COSMIC mapping rules to extract the data movements:

## Rule "MR01"

**Description:**
>  Any object of interest found among the input parameters of the method having one of the annotations "@PostMapping", "@GetMapping", "@RequestMapping" or "@ModelAttribute" is considered as an entry data movement

**Implementation:**
* Java Class: 
 
> org.cfp4j.springmvc.extractor.MappingMethodEntryMovementExtractor  

* Code: 
 
```
  /**
   * 
   * Function that extracts the CtParameter instances associated to the application domain entities
   * and convert it to entry data movements.
   * 
   * An application domain entity type qualified name is started with the provided
   * {@code appRootPackage} that represents the main package of the application to analyze.
   * 
   * This function is the implementation of the COSMIC Mapping Rule #01:
   * <p>
   * Any object of interest found among the input parameters of the method having one of the
   * annotations {@code @PostMapping}, {@code @GetMapping}, {@code @RequestMapping} or
   * {@code @ModelAttribute} is considered as an entry data movement
   * <p>
   * 
   * The function return is a set of DataMovement instances.
   * 
   */
  private static BiFunction<List<CtParameter<?>>, String, Set<DataMovement>> extractAndConvertAppDomainEntitiesToEntryDataMovements =
      (params, rootPackage) -> Optional.ofNullable(params).map(Collection::stream)
          .orElseGet(Stream::empty)
          .filter(p -> SourceCodeUtil.APPLICATION_ENTITY_PARAMETER_PREDICATE.test(rootPackage, p))
          .map(CtParameter::getType).map(CtTypeReference::getSimpleName)
          .map(ep -> new DataMovement(ep.toUpperCase(), Movement.ENTRY))
          .collect(Collectors.toSet());
	          
```

## Rule "MR02 »

**Description:**
> All input parameters having the annotation "@RequestParam" of a method having one of the annotations "@ModelAttribute", "@PostMapping", "@GetMapping" or "@RequestMapping" are considered as a single entry data movement representing a filter of search criteria. A search criterion is excluded from the filter if it represents an attribute of an object of interest found among the input parameters of the method

**Implementation:**
* Java Class:  

> org.cfp4j.springmvc.extractor.MappingMethodEntryMovementExtractor 

* Code:

```
/**
   * Function that extracts the CtParameter instances associated to the primitive type that is
   * annotated with {@code @RequestParam}.
   * 
   * This function is the implementation of the COSMIC Mapping Rule #02:
   * <p>
   * All input parameters having the annotation {@code @RequestParam} of a method having one of the
   * annotations {@code @ModelAttribute}, {@code @PostMapping}, {@code @GetMapping} or
   * {@code @RequestMapping} are considered as a single entry data movement representing a filter of
   * search criteria. A search criterion is excluded from the filter if it represents an attribute
   * of an object of interest found among the input parameters of the method
   * 
   * The function return is an optional of DataMovement.
   * 
   */
  private static BiFunction<CtMethod<?>, String, Optional<DataMovement>> extractAndConvertPrimitiveRequestParametersToEntryDataMovement =
      (method, rootPackage) -> {
        // The method body can contain an application entity setter method that set a primitive
        // entry parameter to an application entity entry parameter.
        // in this case we do not consider the primitive parameter as an entry data movement.
        final Set<CtTypeReference<?>> appEntityParameterTypes =
            extractAppDomainEntities.apply(method.getParameters(), rootPackage);
        final Set<String> appEntitySetterArguments =
            extractAppDomainEntitySetterArgumentName.apply(method, appEntityParameterTypes);
        Set<String> requestParamNames = Optional.ofNullable(method.getParameters())
            .map(Collection::stream).orElseGet(Stream::empty)
            .filter(PRIMITIVE_PARAMETER_PREDICATE.or(PRIMITIVE_OBJECT_PARAMETER_PREDICATE))
            .filter(REQUEST_PARAMETER_PREDICATE)
            .filter(dm -> !appEntitySetterArguments.contains(dm.getSimpleName()))
            .map(CtParameter::getSimpleName).map(String::toUpperCase).collect(Collectors.toSet());

        return requestParamNames.isEmpty() ? Optional.empty()
            : Optional
                .of(new DataMovement("FILTER " + requestParamNames.toString(), Movement.ENTRY));
      };
```

## Rule "MR03"

**Description:**
> All input parameters having the annotation "@PathVariable} of a method having one of the annotations "@ModelAttribute", "@PostMapping", "@GetMapping" or "@RequestMapping" are considered as a single entry data movement representing a filter of search criteria. A search criterion is excluded from the filter if it represents an attribute of an object of interest found among the input parameters of the method.

**Implementation:**
* Java Class: 

> org.cfp4j.springmvc.extractor.MappingMethodEntryMovementExtractor 

* Code:

```
/**
   * Function that extracts the CtParameter instances associated to the primitive type that is
   * annotated with {@code @PathVariable}.
   * 
   * This function is the implementation of the COSMIC Mapping Rule #03:
   * <p>
   * All input parameters having the annotation {@code @PathVariable} of a method having one of the
   * annotations {@code @ModelAttribute}, {@code @PostMapping}, {@code @GetMapping} or
   * {@code @RequestMapping} are considered as a single entry data movement representing a filter of
   * search criteria. A search criterion is excluded from the filter if it represents an attribute
   * of an object of interest found among the input parameters of the method
   * 
   * The function return is a set of DataMovement instances.
   * 
   */
  private static BiFunction<CtMethod<?>, String, Set<DataMovement>> extractAndConvertPrimitivePathVariableParametersToEntryDataMovements =
      (method, rootPackage) -> {
        // The method body can contain an application entity setter method that set a primitive
        // entry parameter to an application entity entry parameter.
        // in this case we do not consider the primitive parameter as an entry data movement.
        final Set<CtTypeReference<?>> appEntityParameterTypes =
            extractAppDomainEntities.apply(method.getParameters(), rootPackage);
        final Set<String> appEntitySetterArguments =
            extractAppDomainEntitySetterArgumentName.apply(method, appEntityParameterTypes);
        return Optional.ofNullable(method.getParameters()).map(Collection::stream)
            .orElseGet(Stream::empty)
            .filter(PRIMITIVE_PARAMETER_PREDICATE.or(PRIMITIVE_OBJECT_PARAMETER_PREDICATE))
            .filter(PATH_VARIABLE_PARAMETER_PREDICATE)
            .filter(dm -> !appEntitySetterArguments.contains(dm.getSimpleName()))
            .map(CtParameter::getSimpleName)
            .map(pp -> new DataMovement(pp.toUpperCase(), Movement.ENTRY))
            .collect(Collectors.toSet());
      };
```

## Rule "MR04"

**Description:**
> Input parameters having the type {@code MultipartFile} of a method having one of the annotations "@ModelAttribute", "@PostMapping", "@GetMapping" or "@RequestMapping" are considered as a entry data movement that represents an attribute of an object of interest.

**Implementation:**
* Java Class:  
 
> org.cfp4j.springmvc.extractor.MappingMethodEntryMovementExtractor 

* Code:

```
/**
   * Function that extracts the CtParameter instances with the type {@code MultipartFile}
   * 
   * This function is the implementation of the COSMIC Mapping Rule #03:
   * <p>
   * Input parameters having the type {@code MultipartFile} of a method having one of the
   * annotations {@code @ModelAttribute}, {@code @PostMapping}, {@code @GetMapping} or
   * {@code @RequestMapping} are considered as a entry data movement that represents an attribute of
   * an object of interest.
   * 
   * The function returns a set of entry DataMovement instances.
   * 
   */
  private static Function<List<CtParameter<?>>, Set<DataMovement>> extractAndConvertMultipartFileParametersToEntryDataMovements =
      params -> Optional.ofNullable(params).map(Collection::stream).orElseGet(Stream::empty)
          .filter(p -> "MultipartFile".equals(p.getType().getSimpleName()))
          .map(p -> new DataMovement(p.getSimpleName().toUpperCase(), Movement.ENTRY))
          .collect(Collectors.toSet());
```

## Rule "MR05"

**Description:**
> Each entry data movement related to method having one of the annotations "@PostMapping", "@GetMapping" or "@RequestMapping" must not be an exit data movement related to a method having the annotation "@ModelAttribute".

**Implementation:**
* Java Class:   

> org.cfp4j.springmvc.analyzer.MappingMethodAnalyzer 

* Code:

```
  /**
   * 
   * Function that exclude the provided {@code modelAttributMethodExits} from the provided data
   * movement set.
   * 
   * This function is the implementation of the COSMIC Mapping Rule #05:
   * <p>
   * Each entry data movement related to method having one of the annotations {@code @PostMapping},
   * {@code @GetMapping} or {@code @RequestMapping} must not be an exit data movement related to a
   * method having the annotation {@code @ModelAttribute}
   * 
   * The function returns a set of DataMovement instances.
   * 
   */
  private static BiFunction<Set<DataMovement>, Set<String>, Set<DataMovement>> excludeModelAttributMethodExitDataMovements =
      (dataMovements, modelAttributMethodExits) -> Optional.ofNullable(dataMovements)
          .map(Collection::stream).orElseGet(Stream::empty)
          .filter(dt -> !modelAttributMethodExits.contains(dt.getName()))
          .collect(Collectors.toSet());
```

## Rule "MR06"

**Description:**
> If at least one Read or Write data movement has been recorded, and no Entry data movement has been associated to a method having one of the annotations "@PostMapping", "@GetMapping" or "@RequestMapping", then an Entry data movement a trigger for this functional process must be added.

**Implementation:**
* Java Class:   

> org.cfp4j.springmvc.analyzer.MappingMethodAnalyzer 

* Code:

```
/**
   * 
   * Function that returns an optional instance including an entry data movement representing a
   * functional user trigger if it is applied, otherwise the return will be an empty Optional
   * instance.
   * 
   * * This function is the implementation of the COSMIC Mapping Rule #06:
   * <p>
   * If at least one Read or Write data movement has been recorded, and no Entry data movement has
   * been associated to a method having one of the annotations {@code @PostMapping},
   * {@code @GetMapping} or {@code @RequestMapping}, then an Entry data movement a trigger for this
   * functional process must be added.
   * 
   * The function returns an Optional instance.
   * 
   */
   private static BiFunction<Set<DataMovement>, Set<DataMovement>, Optional<DataMovement>> extractOptionalFonctionalUserTrigger =
      (entries, readwrites) -> {
        /*
         * The FU and the data that initiate a FP aren't always obvious, however according to the
         * COSMIC rules, there must be a FU and an Entry (otherwise the result isn't a COSMIC
         * measurement).
         */
        final BiPredicate<Set<DataMovement>, Set<DataMovement>> triggerPredicate =
            (e, rw) -> (e.isEmpty() && !rw.isEmpty());
        return triggerPredicate.test(entries, readwrites)
            ? Optional.of(new DataMovement("TRIGGER", Movement.ENTRY))
            : Optional.empty();
      };
```

## Rule "MR07"

**Description:**
> If a method having one of the annotations "@PostMapping", "@GetMapping" or "@RequestMapping", has at least one input parameter annotated with the annotation "@Valid" or is "BindingResult" instance or there is a call to a "Delete" method during the execution of this method, then an Exit data movement representing an output message related to an input parameter validation message or a deletion confirmation message.

**Implementation:**
* Java Class:   

> org.cfp4j.springmvc.analyzer.MappingMethodAnalyzer

* Code:

```
/**
   * Function that returns an optional instance including an entry data movement representing a
   * functional user message if it exists, otherwise the return will be an empty Optional instance.
   * 
   * This function is the implementation of the COSMIC Mapping Rule #07:
   * <p>
   * If a method having one of the annotations {@code @PostMapping}, {@code @GetMapping} or
   * {@code @RequestMapping}, has at least one input parameter annotated with the annotation
   * {@code @Valid} or is {@code BindingResult} instance or there is a call to a "Delete" method
   * during the execution of this method, then an Exit data movement representing an output message
   * related to an input parameter validation message or a deletion confirmation message.
   * 
   * returned. The function returns an Optional instance.
   */
  private BiFunction<Map<String, CtMethod<?>>, CtMethod<?>, Optional<DataMovement>> extractValidationOrConfirmationMessageDataGroup =
      (appCtMethods, method) -> {
        List<CtParameter<?>> entryParams = method.getParameters();
        final boolean isValidAnnotationExists =
            Optional.ofNullable(entryParams).map(Collection::stream).orElseGet(Stream::empty)
                .anyMatch(p -> p.hasAnnotation(Valid.class));
        final boolean isBindingResultExists =
            Optional.ofNullable(entryParams).map(Collection::stream).orElseGet(Stream::empty)
                .anyMatch(p -> "BindingResult".equals(p.getType().getSimpleName()));

        if (isValidAnnotationExists || isBindingResultExists) {
          return Optional.of(new DataMovement(MESSAGE_DATA_GROUP_NAME, Movement.EXIT));
        }

        if (DELETE_REPOSITORY_INVOCATIONS_EXISTS_PREDICATE.test(appCtMethods, method)) {
          return Optional.of(new DataMovement(MESSAGE_DATA_GROUP_NAME, Movement.EXIT));
        }
        return Optional.empty();
      };
```

## Rule "MR08"

**Description:**
> Each object of interest added as a new element to one of the instances "Map", "Model", "ModelMap" or "ModelAndView" in the body of a method having one of the annotations "@ModelAttribute", "@PostMapping", "@GetMapping" or "@RequestMapping", is considered as an Exit data movement. The object of interest instance must not be a local instance.

**Implementation:**
* Java Class:   

> org.cfp4j.springmvc.extractor.MappingMethodExitMovementExtractor 

* Code:

```
/**
   * Function that extracts the exit data movements from the provided method.
   * 
   * If the provided method has SpringMVC Model attributes instance as entry parameter, the Exit
   * data movement info is extracted from a call of the "put" or "addAttribute" model actions,
   * otherwise it will be extracted from a call of the "addObject" action related to a local
   * instance of the type "ModelAndView".
   * 
   * The data movement need to be a type of an application domain entity witch the qualified name is
   * prefixed with {@code appRootPackage}.
   * 
   * This function is the implementation of the COSMIC Mapping Rule #08:
   * <p>
   * Each object of interest added as a new element to one of the instances {@code Map},
   * {@code Model}, {@code ModelMap} or {@code ModelAndView} in the body of a method having one of
   * the annotations {@code @ModelAttribute}, {@code @PostMapping}, {@code @GetMapping} or
   * {@code @RequestMapping}, is considered as an Exit data movement. The object of interest
   * instance must not be a local instance
   * 
   * 
   * The function returns a set of the data movements.
   */
  private BiFunction<CtMethod<?>, String, Set<DataMovement>> extractExitDataMovementsFromModelInstance =
      (method, rootPackage) -> {
        final Set<String> appEntityLocalInstanceVariableNames =
            extractAppEntityLocalInstanceVariableNamesFromMethodBody.apply(method, rootPackage);
        final Set<DataMovement> modelExitDataMovements =
            extractExitDatMovementsBasedOnModelInstanceAmongMethodEntryParameters(method,
                appEntityLocalInstanceVariableNames);
        final Set<DataMovement> modelAndViewExitDataMovements =
            extractExitDatMovementsBasedOnModelAndViewLocalInstanceInMethodBody(method,
                appEntityLocalInstanceVariableNames);
        final Set<DataMovement> localInvocationExits =
            extractExitDataMovementFromLocalInvocation(method);
        return Stream
            .concat(Stream.concat(modelExitDataMovements.stream(),
                modelAndViewExitDataMovements.stream()), localInvocationExits.stream())
            .collect(Collectors.toSet());
      };
```

## Rule "MR09"

**Description:**
> Each method having the annotation "@ResponseBody" and one of the annotations "@PostMapping", "@GetMapping" or "@RequestMapping", is a method returning an object of interest considered as a Exit data movement.

**Implementation:**
* Java Class:   

> org.cfp4j.springmvc.extractor.MappingMethodExitMovementExtractor 

* Code:

```
/**
   * Function that extracts the exit data movement from the provided method. The method must have
   * the {@code @ResponseBody} method annotation.
   * 
   * This function is the implementation of the COSMIC Mapping Rule #09:
   * <p>
   * Each method having the annotation {@code @ResponseBody} and one of the annotations
   * {@code @PostMapping}, {@code @GetMapping} or {@code @RequestMapping}, is a method returning an
   * object of interest considered as an Exit data movement.
   * 
   * The function returns a set of the data movements.
   */
  private Function<CtMethod<?>, Set<DataMovement>> extractExitDataMovementForMappingMethodWithReponseBodyAnnotation =
      method -> {
        CtTypeReference<?> methodType = method.getType();
        if (methodType.getSimpleName().equals("String")) {
          CtStatement lastStatment = method.getBody().getLastStatement();
          if (!(lastStatment instanceof CtReturn)) {
            LOGGER.error("The last Statment is not an instance of CtReturn for the method '{}'",
                method.getSimpleName());
            return Collections.emptySet();
          }
          CtReturn<?> returnStatment = (CtReturn<?>) lastStatment;
          if (returnStatment.getReturnedExpression() instanceof CtInvocation) {
            CtInvocation<?> returnedExpression =
                (CtInvocation<?>) returnStatment.getReturnedExpression();
            String returnMethodName = returnedExpression.getExecutable().getSimpleName();
            String returnMethodArguments = returnedExpression.getArguments().isEmpty() ? ""
                : returnedExpression.getArguments().stream().map(e -> e.toString())
                    .collect(Collectors.joining(", ", "[", "]"));
            return Collections.singleton(
                new DataMovement(returnMethodName + returnMethodArguments, Movement.EXIT));
          }

          return Collections.singleton(
              new DataMovement(returnStatment.getReturnedExpression().toString(), Movement.EXIT));
        }

        boolean isIterableType =
            SourceCodeUtil.ITERABLE_TYPES.contains(methodType.getSimpleName().toUpperCase());
        if (isIterableType) {
          methodType = methodType.getActualTypeArguments().get(0);
        }
        String dataGroupName = String.format("%s%s", methodType.getSimpleName().toUpperCase(),
            isIterableType ? DATA_GROUP_ITERABLE_TYPE_NAME_SUFFIX : "");
        return Collections.singleton(new DataMovement(dataGroupName, Movement.EXIT));
      };
```

## Rule "MR10"

**Description:**
> Each call to one of the "findById", "existsById", "findAll", "getOne", "count" or "findAllById" methods of a Spring Data Repository class, recursively from a method with one of the annotations "@ModelAttribute", "@PostMapping", "@GetMapping" or "@RequestMapping", is considered as a Read data movement.

**Implementation:**
* Java Class:  
 
> org.cfp4j.springmvc.extractor.SpringDataReadWriteMovementExtractor 

* Code:

```
/**
   * Function that extracts the Read data movement from the provided {@code CtExecutableReference}
   * representing a Spring Data Repository invocation (or method).
   * 
   * 
   * If the method name is among of the defined Spring Data framework that presents a read method,
   * then the Function returns an Optional instance with a Read data movement instance, otherwise an
   * empty Option instance will be returned.
   * 
   * If the method name starts with the string "findAll", the data group name is the declared
   * repository domain entity name concatenated with the string "_LIST".
   * 
   * This function is the implementation of the COSMIC Mapping Rule #10:
   * <p>
   * Each call to one of the "findById", "existsById", "findAll", "getOne", "count" or "findAllById"
   * methods of a Spring Data Repository class, recursively from a method with one of the
   * annotations {@code @ModelAttribute}, {@code @PostMapping}, {@code @GetMapping} or
   * {@code @RequestMapping}, is considered as a Read data movement.
   * 
   */
  private static Function<CtExecutableReference<?>, Optional<DataMovement>> extractReadDataGroupFromPredefinedRepositoryInvocation =
      executableRef -> {

        final String methodName = executableRef.getSimpleName();
        boolean isRepositoryReadMethod =
            SPRING_DATA_REPOSITORY_READ_METHOD_NAMES.stream().anyMatch(methodName::equals);
        if (isRepositoryReadMethod) {
          CtTypeReference<?> typeRef =
              ((CtFieldRead<?>) ((CtInvocation<?>) executableRef.getParent()).getTarget())
                  .getVariable().getType().getSuperInterfaces().iterator().next();
          String domainEntity =
              typeRef.getActualTypeArguments().get(0).getSimpleName().toUpperCase();
          return Optional.of(new DataMovement(String.format("%s%s", domainEntity,
              methodName.startsWith(SPRING_DATA_JPA_REPOSITORY_COLLECTION_READ_METHOD_NAME_PREFIX)
                  ? DATA_GROUP_ITERABLE_TYPE_NAME_SUFFIX
                  : ""),
              Movement.READ));
        }
        return Optional.empty();
      };
```

## Rule "MR11"

**Description:**
> Each call to one of the methods "save", "saveAll", "saveAndFlush", "deleteInBatch", "deleteAllInBatch", "deleteById", "delete" or "deleteAll" of a Spring Data Repository class, recursively from a method having one of the annotations "@ModelAttribute", "@PostMapping", "@GetMapping" or "@RequestMapping", is considered as a Write data movement.

**Implementation:**
* Java Class:   

> org.cfp4j.springmvc.extractor.SpringDataReadWriteMovementExtractor 

* Code:

```
  /**
   * Function that extracts the Write data movement from the provided {@code CtExecutableReference}
   * representing a Spring Data Repository invocation (or method).
   * 
   * 
   * If the method name is among of the defined Spring Data framework that presents a persist
   * method, then the Function returns an Optional instance with a Write data movement instance,
   * otherwise an empty Option instance will be returned.
   * 
   * 
   * This function is the implementation of the COSMIC Mapping Rule #11:
   * <p>
   * Each call to one of the methods "save", "saveAll", "saveAndFlush", "deleteInBatch",
   * "deleteAllInBatch", "deleteById", "delete" or "deleteAll" of a Spring Data Repository class,
   * recursively from a method having one of the annotations {@code @ModelAttribute},
   * {@code @PostMapping}, {@code @GetMapping} or {@code @RequestMapping}, is considered as a Write
   * data movement.
   * 
   */
  private static Function<CtExecutableReference<?>, Optional<DataMovement>> extractWriteDataGroupFromPredefinedRepositoryInvocation =
      executableRef -> {

        final String methodName = executableRef.getSimpleName();
        boolean isRepositoryWriteMethod =
            SPRING_DATA_REPOSITORY_WRITE_METHOD_NAMES.stream().anyMatch(methodName::equals);
        if (isRepositoryWriteMethod) {
          CtTypeReference<?> typeRef =
              ((CtFieldRead<?>) ((CtInvocation<?>) executableRef.getParent()).getTarget())
                  .getVariable().getType().getSuperInterfaces().iterator().next();
          String domainEntity =
              typeRef.getActualTypeArguments().get(0).getSimpleName().toUpperCase();
          return Optional.of(new DataMovement(domainEntity, Movement.WRITE));
        }
        return Optional.empty();
      };
```

## Rule "MR12"

**Description:**
> Each call to a custom method of a class implementing the Spring Data Repository interface, recursively from a method having one of the annotations "@ModelAttribute", "@PostMapping", "@GetMapping" or "@RequestMapping", is considered a data movement. If this method has a return type then it is a Read data movement type is in "Read", otherwise (void type), it is a Write data movement.

**Implementation:**
* Java Class:  
 
> org.cfp4j.springmvc.extractor.SpringDataReadWriteMovementExtractor 

* Code:

```
	/**
	   * Function that extracts the data movement from the provided custom repository method. A custom
	   * method is a method that was defined in the current source code and not a override spring data
	   * repository method.
	   * 
	   * If the provided invocation does not have a return type (it is a void type), so this invocation
	   * is considered a write movement, the data group name is the method argument expression.
	   * 
	   * The invocation that has return type, it is considered a read movement, the data group name is
	   * the type simple name for non generic return type, otherwise if the generic type is an iterator
	   * type, the data group is the concatenation of the type simple name and the string "_LIST".
	   * 
	   * This function is the implementation of the COSMIC Mapping Rule #12:
	   * <p>
	   * Each call to a custom method of a class implementing the Spring Data Repository interface,
	   * recursively from a method having one of the annotations {@code @ModelAttribute},
	   * {@code @PostMapping}, {@code @GetMapping} or {@code @RequestMapping}, is considered a data
	   * movement. If this method has a return type then it is a Read data movement type is in "Read",
	   * otherwise (void type), it is a Write data movement.
	   * 
	   */
	  private static Function<CtInvocation<?>, DataMovement> extractDataGroupFromRepositoryCustomInvocation =
	      i -> {
	        if (INVOCATION_WITH_VOID_TYPE_PREDICATE.test(i)) {
	          // Extract the method arguments, the data group name is the argument names
	          Set<String> argNames = i.getArguments().stream()
	              .map(e -> e.toString().replace(".get", "_").replace("()", "").toUpperCase())
	              .collect(Collectors.toSet());
	          return new DataMovement(
	              argNames.size() == 1 ? argNames.iterator().next() : argNames.toString(),
	              Movement.WRITE);
	        }
	
	        if (i.getExecutable().getType().isPrimitive()) {
	          return new DataMovement(i.getExecutable().getSimpleName().toUpperCase(), Movement.READ);
	        }
	
	        if (i.getExecutable().getType().getActualTypeArguments().isEmpty()) {
	          return new DataMovement(i.getType().getSimpleName().toUpperCase(), Movement.READ);
	        }
	
	
	        String typeName = i.getExecutable().getType().getSimpleName();
	        boolean isIterableType = SourceCodeUtil.ITERABLE_TYPES.contains(typeName.toUpperCase());
	        String dataGroupName = String.format("%s%s",
	            i.getExecutable().getType().getActualTypeArguments().get(0).getSimpleName(),
	            isIterableType ? DATA_GROUP_ITERABLE_TYPE_NAME_SUFFIX : "");
	        return new DataMovement(dataGroupName.toUpperCase(), Movement.READ);
	      };
```


