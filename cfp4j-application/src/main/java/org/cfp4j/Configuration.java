package org.cfp4j;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

public class Configuration {

    private final String projectName;
    private final String sourceCodePath;
    private final  String sourceCodeMainPackage;
    private final  String reportFormats;


    public Configuration() throws IOException {
        Properties properties = loadProperties();
        this.projectName = Objects.requireNonNull(properties.getProperty("project.name"));
        this.sourceCodePath = Objects.requireNonNull(properties.getProperty("source.code.path"));
        this.sourceCodeMainPackage = Objects.requireNonNull(properties.getProperty("source.code.main.package"));
        this.reportFormats = Objects.requireNonNull(properties.getProperty("report.formats"));
    }

    public String getProjectName() {
        return projectName;
    }

    public String getSourceCodePath() {
        return sourceCodePath;
    }

    public String getSourceCodeMainPackage() {
        return sourceCodeMainPackage;
    }
    
    public String getReportFormats() {
        return reportFormats;
    }

    private Properties loadProperties() throws IOException{
        try(FileInputStream in = new FileInputStream(Objects.requireNonNull(System.getProperty("app.config.location")))){
            Properties defaultProps = new Properties();
            defaultProps.load(in);
            return defaultProps;
        }

    }
}
