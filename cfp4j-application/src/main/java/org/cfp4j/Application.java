package org.cfp4j;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.cfp4j.entity.MeasurementResult;
import org.cfp4j.entity.Reference;
import org.cfp4j.report.impl.ReportComposite;
import org.cfp4j.report.impl.ReportComposite.ReportFormat;
import org.cfp4j.springmvc.SpringMvcCfpCalculator;

public class Application {

	public static void main(String[] args) throws IOException{
		final Configuration configuration = new Configuration();
		final String reportDirectoryPath = Objects.requireNonNull(System.getProperty("app.report.location"));
		final Instant start = Instant.now();
		ICfpCalculator calculator = new SpringMvcCfpCalculator(configuration.getSourceCodeMainPackage());
		Set<Reference> references = calculator.calculate(configuration.getSourceCodePath());
		final Instant end = Instant.now();
		final long duration = Duration.between(start, end).toMillis();
		MeasurementResult measurementResult = new MeasurementResult(configuration.getProjectName(), start.toEpochMilli(), duration, references);
		// generate reports
		Set<ReportFormat> reportFormats = Arrays.asList(configuration.getReportFormats().split(",")).stream().map(String::toUpperCase).map(ReportFormat::valueOf).collect(Collectors.toSet());
        new ReportComposite(reportFormats).generate(measurementResult, reportDirectoryPath);
	}

}
	