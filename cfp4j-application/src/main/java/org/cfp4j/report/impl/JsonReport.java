package org.cfp4j.report.impl;

import java.io.BufferedWriter;
import java.io.FileWriter;

import org.cfp4j.entity.MeasurementResult;
import org.cfp4j.report.IReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonReport implements IReport {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ExcelReport.class);
	
	private final ObjectMapper mapper = new ObjectMapper();
	@Override
	public void generate(final MeasurementResult report,final String outputFilePath) {
		final String filePath = String.format("%s\\cosmic-metric-%s-%s.json",outputFilePath, report.getProject().replace(" ", "_"), report.getStartTime());
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath));) {
			writer.write(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(report));
		} catch (Exception e) {
			LOGGER.error("Error while generation the JSON report", e);
		}
	}
	
}
