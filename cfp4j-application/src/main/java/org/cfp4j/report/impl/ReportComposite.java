package org.cfp4j.report.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.cfp4j.entity.MeasurementResult;
import org.cfp4j.report.IReport;

public class ReportComposite implements IReport {
	
	private static final Map<ReportFormat, IReport> reports = new HashMap<ReportComposite.ReportFormat, IReport>();
	private final Set<ReportFormat> reportFormats;
	
	static {
		reports.put(ReportFormat.EXCEL, new ExcelReport());
		reports.put(ReportFormat.JSON, new JsonReport());
	}
	
	
	public ReportComposite(Set<ReportFormat> reportFormats) {
		this.reportFormats = Objects.requireNonNull(reportFormats);
	}

	@Override
	public void generate(MeasurementResult report, String outputFilePath) {
		this.reportFormats.stream().forEach(r->reports.get(r).generate(report, outputFilePath));		
	}

	public enum ReportFormat  {
		EXCEL, JSON;
	}
}
