package org.cfp4j.report.impl;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.cfp4j.entity.DataGroup;
import org.cfp4j.entity.FunctionalProcess;
import org.cfp4j.entity.MeasurementResult;
import org.cfp4j.entity.Movement;
import org.cfp4j.entity.Reference;
import org.cfp4j.report.IReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExcelReport implements IReport {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExcelReport.class);
	
	@Override
	public void generate(final MeasurementResult report,final String outputFilePath) {
		final String filePath = String.format("%s\\cosmic-metric-%s-%s.xlsx",outputFilePath, report.getProject().replace(" ", "_"), report.getStartTime());
		
		try (InputStream in = ExcelReport.class.getResourceAsStream("/cosmic-metric-template.xlsx");
				Workbook workbook = WorkbookFactory.create(in);
				FileOutputStream outputStream = new FileOutputStream(filePath);) {
			
			//cell styles
			Font boldFont = workbook.createFont();
			boldFont.setBold(true);
			CellStyle refBackgroundStyle = workbook.createCellStyle();
			refBackgroundStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
			refBackgroundStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			refBackgroundStyle.setFont(boldFont);
			CellStyle fpBackgroundStyle = workbook.createCellStyle();
			fpBackgroundStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
			fpBackgroundStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			fpBackgroundStyle.setFont(boldFont);
			
			// metrics
			Object[][] metrics = extractMetrics(report.getReferences());
			// Metric sheet
			Sheet sheet = workbook.getSheetAt(0);
			// update Summary section
			sheet.getRow(5).getCell(5).setCellValue(report.getProject());
			sheet.getRow(6).getCell(5).setCellValue(formatStartDate(report.getStartTime()));
			sheet.getRow(7).getCell(5).setCellValue(formatDuration(report.getDuration()));
			
			// sums
			long entries = report.getReferences().stream().map(ref->ref.getFunctionalProcesses()).flatMap(Set::stream).map(fp->fp.getDataGroupSet()).flatMap(Set::stream).map(dt->dt.getMovements()).flatMap(Set::stream).filter(mv -> mv.equals(Movement.ENTRY)).count();
			long exits = report.getReferences().stream().map(ref->ref.getFunctionalProcesses()).flatMap(Set::stream).map(fp->fp.getDataGroupSet()).flatMap(Set::stream).map(dt->dt.getMovements()).flatMap(Set::stream).filter(mv -> mv.equals(Movement.EXIT)).count();
			long reads = report.getReferences().stream().map(ref->ref.getFunctionalProcesses()).flatMap(Set::stream).map(fp->fp.getDataGroupSet()).flatMap(Set::stream).map(dt->dt.getMovements()).flatMap(Set::stream).filter(mv -> mv.equals(Movement.READ)).count();
			long writes = report.getReferences().stream().map(ref->ref.getFunctionalProcesses()).flatMap(Set::stream).map(fp->fp.getDataGroupSet()).flatMap(Set::stream).map(dt->dt.getMovements()).flatMap(Set::stream).filter(mv -> mv.equals(Movement.WRITE)).count();
			long total = entries + exits + reads + writes;
			
			sheet.getRow(9).getCell(5).setCellValue((Long) total);
			sheet.getRow(9).getCell(8).setCellValue((Long) entries);
			sheet.getRow(9).getCell(10).setCellValue((Long) reads);
			sheet.getRow(9).getCell(12).setCellValue((Long) writes);
			sheet.getRow(9).getCell(14).setCellValue((Long) exits);
			
			
			int rowStartIndex = 14;
			for (int i = 0; i < metrics.length; i++) {
				Row row = sheet.createRow(i + rowStartIndex);
				int startMergeColumn = 0;
				for (int j = 0; j < metrics[i].length; j++) {
					Object field = metrics[i][j];
					int cellToMerge = j > 2 ? 1 : 3; // (0-based)
					Cell cell = row.createCell(startMergeColumn);
					if ((metrics[i][0] instanceof String) && ((String)metrics[i][0]) != null && ((String)metrics[i][0]).trim().length() > 0) {
						cell.setCellStyle(refBackgroundStyle);
					} else if ((metrics[i][1] instanceof String) && ((String)metrics[i][1]) != null && ((String)metrics[i][1]).trim().length() > 0) {
						cell.setCellStyle(fpBackgroundStyle);
					}
					if (field instanceof String) {
						cell.setCellValue((String) field);
					} else if (field instanceof Integer) {
						cell.setCellValue((Integer) field);
					} else if (field instanceof Long) {
						cell.setCellValue((Long) field);
					}
					
					sheet.addMergedRegion(new CellRangeAddress(
							i + rowStartIndex, //first row (0-based)
							i + rowStartIndex, //last row  (0-based)
							startMergeColumn, //first column (0-based)
							startMergeColumn + cellToMerge  //last column  (0-based)
					));		
					startMergeColumn +=cellToMerge+1;
				}
			}
			// force
			workbook.setForceFormulaRecalculation(true);
			// write data
			workbook.write(outputStream);
		} catch (IOException e) {
			LOGGER.error("Error while generation the Excel report", e);
		}

	}

	private Object[][] extractMetrics(Set<Reference> references) {
		List<Object[]> data = new ArrayList<Object[]>();
		for (Reference ref : references) {
			// ref line
			long refEntries = ref.getFunctionalProcesses().stream().map(fp->fp.getDataGroupSet()).flatMap(Set::stream).map(dt->dt.getMovements()).flatMap(Set::stream).filter(mv -> mv.equals(Movement.ENTRY)).count();
			long refExits = ref.getFunctionalProcesses().stream().map(fp->fp.getDataGroupSet()).flatMap(Set::stream).map(dt->dt.getMovements()).flatMap(Set::stream).filter(mv -> mv.equals(Movement.EXIT)).count();
			long refReads = ref.getFunctionalProcesses().stream().map(fp->fp.getDataGroupSet()).flatMap(Set::stream).map(dt->dt.getMovements()).flatMap(Set::stream).filter(mv -> mv.equals(Movement.READ)).count();
			long refWrites = ref.getFunctionalProcesses().stream().map(fp->fp.getDataGroupSet()).flatMap(Set::stream).map(dt->dt.getMovements()).flatMap(Set::stream).filter(mv -> mv.equals(Movement.WRITE)).count();
			data.add(new Object[] { ref.getName(), "", "", refEntries, refReads, refWrites, refExits, ref.functionalPoints() });
			for (FunctionalProcess funcPro : ref.getFunctionalProcesses()) {
				// fp line
				long entries = funcPro.getDataGroupSet().stream().map(dt->dt.getMovements()).flatMap(Set::stream).filter(mv -> mv.equals(Movement.ENTRY)).count();
				long exits = funcPro.getDataGroupSet().stream().map(dt->dt.getMovements()).flatMap(Set::stream).filter(mv -> mv.equals(Movement.EXIT)).count();
				long reads = funcPro.getDataGroupSet().stream().map(dt->dt.getMovements()).flatMap(Set::stream).filter(mv -> mv.equals(Movement.READ)).count();
				long writes = funcPro.getDataGroupSet().stream().map(dt->dt.getMovements()).flatMap(Set::stream).filter(mv -> mv.equals(Movement.WRITE)).count();
				data.add(new Object[] { "", funcPro.getName(), "", entries, reads, writes, exits, funcPro.functionalPoints() });
				data.addAll(buildDataGroupLines(funcPro.getDataGroupSet()));
			}

		}

		return data.stream().toArray(Object[][]::new);
	}

	private List<Object[]> buildDataGroupLines(Set<DataGroup> dataGroups) {
		return dataGroups.stream().map(dg-> new Object[] { "", "", dg.getName(), 
					dg.getMovements().stream().filter(mv -> mv.equals(Movement.ENTRY)).count(), 
					dg.getMovements().stream().filter(mv -> mv.equals(Movement.READ)).count(), 
					dg.getMovements().stream().filter(mv -> mv.equals(Movement.WRITE)).count(), 
					dg.getMovements().stream().filter(mv -> mv.equals(Movement.EXIT)).count(),
					dg.getMovements().size()}
		).collect(Collectors.toList());
	}

	private String formatStartDate(long epoch) {
		ZoneId zone = ZoneId.systemDefault();
		DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy, hh:mm:ss a").withZone(zone);
		return df.format(Instant.ofEpochMilli(epoch));
	}

	private String formatDuration(long durationInMil) {
		final long hr = TimeUnit.MILLISECONDS.toHours(durationInMil)
				- TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(durationInMil));
		final long min = TimeUnit.MILLISECONDS.toMinutes(durationInMil)
				- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(durationInMil));
		final long sec = TimeUnit.MILLISECONDS.toSeconds(durationInMil)
				- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(durationInMil));
		final long ms = TimeUnit.MILLISECONDS.toMillis(durationInMil)
				- TimeUnit.SECONDS.toMillis(TimeUnit.MILLISECONDS.toSeconds(durationInMil));
		
		return String.format("%s:%s:%s.%s", hr < 10 ? "0"+hr:hr, min < 10 ? "0"+min:min,sec < 10 ? "0"+sec:sec,ms);
	}
}
