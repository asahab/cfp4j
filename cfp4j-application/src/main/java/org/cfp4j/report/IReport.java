package org.cfp4j.report;

import org.cfp4j.entity.MeasurementResult;

public interface IReport {

	public void generate(final MeasurementResult report,final String outputFilePath);
}
