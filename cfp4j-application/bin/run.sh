#!/bin/sh
set -o nounset
set -o errexit
 
# Set the default APP_HOME
APP_HOME=$(cd ..; pwd)
export APP_HOME
 
#
# Specify options to pass to the Java VM.
#
JAVA_OPTS="-Xms1024m -Xmx2048m -Xmn1360m"
JAVA_OPTS="$JAVA_OPTS -Dapp.report.location=%APP_HOME%/report -Dapp.config.location=$APP_HOME/conf/application.properties"
 
# Display summary of the environment
echo "========================================================================="
echo ""
echo "  MOCK CFNA SOAP SERVICES Environment"
echo ""
echo "  MOCK_HOME: $APP_HOME"
echo ""
echo "  JAVA: $JAVA_HOME"
echo ""
echo "  JAVA_OPTS: $JAVA_OPTS"
echo ""
echo "========================================================================="
echo ""
 
# Launch the application in the background
exec $JAVA $JAVA_OPTS \
      -jar $APP_HOME/cfp4j-application.jar \
      "$@" &
COSMIC_CFP4J_APP_PID=$!
