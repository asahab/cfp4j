@echo off 
rem ************************************************************
rem Start Script for COSMIC CFP4J APP
rem ************************************************************

setlocal

rem Set the default APP_HOME
for %%i in ("%~dp0..") do set "APP_HOME=%%~fi"

rem Specify options to pass to the Java VM.
set "JAVA_OPTS=-Xms1024m -Xmx2048m -Xmn1360m"
set "JAVA_OPTS=%JAVA_OPTS% -Dapp.report.location=%APP_HOME%\report -Dapp.config.location=%APP_HOME%\conf\application.properties"

rem Display summary of the environment
echo =========================================================================
echo(
echo   COSMIC CFP4J APP Environment
echo(
echo   APP_HOME: "%APP_HOME%"
echo(
echo   JAVA_HOME: "%JAVA_HOME%"
echo(
echo   JAVA_OPTS: "%JAVA_OPTS%"
echo(
echo =========================================================================
echo(

rem Launch the application
java %JAVA_OPTS% -jar "%APP_HOME%\cfp4j-application.jar"

pause

exit /b 0

goto :eof