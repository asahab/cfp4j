package org.cfp4j;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collections;
import java.util.Set;

import org.cfp4j.entity.Reference;
import org.junit.Test;

import spoon.reflect.CtModel;

public class ICfpCalculatorTest {
	
	@Test(expected = NullPointerException.class)
    public void testNullPath() {
		new CfpCalculator().calculate(null);
	}

	@Test
    public void testSuccessProjectSizing() {
		Set<Reference> refs = new CfpCalculator().calculate("src/test/resources/source-code/maven-project");
		assertThat(refs).isNotNull();
	}
	
	
	class CfpCalculator implements ICfpCalculator {
		@Override
		public Set<Reference> doCalculation(CtModel model) {
			return Collections.emptySet();
		}
		
	}

}