package org.cfp4j.helpers;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;

import spoon.Launcher;
import spoon.MavenLauncher;

public class SpoonLauncherFactoryTest {
	
	@Test(expected = NullPointerException.class)
    public void testNullPath() {
		new SpoonLauncherFactory().getLauncher(null);
	}
	
	@Test
    public void testMavenLauncherInstance() {
		Path sourcePath = Paths.get("src/test/resources/source-code/maven-project");
		Launcher launcher = new SpoonLauncherFactory().getLauncher(sourcePath);
		assertThat(launcher).isNotNull().isInstanceOf(MavenLauncher.class);
	}
	
	@Test
    public void testStandardLauncherInstance() {
		Path sourcePath = Paths.get("src/test/resources/source-code/Java-project");
		Launcher launcher = new SpoonLauncherFactory().getLauncher(sourcePath);
		assertThat(launcher).isNotNull().isInstanceOf(Launcher.class);
	}

}
