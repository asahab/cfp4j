package org.cfp4j.helpers;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import spoon.reflect.CtModel;

public class SpoonCompileTimeModelBuilderTest {
	
	@Test(expected = NullPointerException.class)
    public void testNullProjectPath() {
		SpoonCompileTimeModelBuilder.projectPath(null);
	}
	
	@Test(expected = RuntimeException.class)
    public void testInvalidProjectPath() {
		SpoonCompileTimeModelBuilder.projectPath("src/test/resources/source-code/maven-projectXXXXX").build();
	}
	
	@Test
    public void testSuccessMavenProjectBuild() {
		CtModel ctModel = SpoonCompileTimeModelBuilder.projectPath("src/test/resources/source-code/maven-project").build();
		assertThat(ctModel).isNotNull();
	}
	
	@Test
    public void testSuccessJavaProjectBuild() {
		CtModel ctModel = SpoonCompileTimeModelBuilder.projectPath("src/test/resources/source-code/java-project").build();
		assertThat(ctModel).isNotNull();
	}
}
