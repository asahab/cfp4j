package org.cfp4j.entity;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;


public class FunctionalProcess extends NamedEntity{

	private final Set<DataGroup> dataGroupSet;

	public FunctionalProcess(String name) {
		super(name);
		this.dataGroupSet = new HashSet<>();
	}


	public Set<DataGroup> getDataGroupSet() {
		return dataGroupSet;
	}

	public void addDataGroupMovement(String dataGroupName, Movement movement) {
		Optional<DataGroup> optionalDataGroup = dataGroupSet.stream()
				.filter(dg -> dg.getName().equalsIgnoreCase(dataGroupName))
				.reduce((u, v) -> {
					throw new IllegalStateException("More than one data group name found");
				});
		
		if (optionalDataGroup.isPresent()) {
			optionalDataGroup.get().addMovement(movement);
		} else {
			DataGroup dg = new DataGroup(dataGroupName);
			dg.addMovement(movement);
			dataGroupSet.add(dg);
		}
	}

	public int functionalPoints() {
		return dataGroupSet.stream().mapToInt(DataGroup::functionalPoints).sum();
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
}
