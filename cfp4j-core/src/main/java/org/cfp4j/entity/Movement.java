package org.cfp4j.entity;

public enum Movement {
	ENTRY, EXIT, READ, WRITE;
}