package org.cfp4j.entity;

import java.util.HashSet;
import java.util.Set;

public class Reference extends NamedEntity{

	private final Set<FunctionalProcess> functionalProcesses;

	public Reference(String name) {
		this(name, new HashSet<FunctionalProcess>());
	}

	public Reference(String name, Set<FunctionalProcess> functionalProcesses) {
		super(name);
		this.functionalProcesses = functionalProcesses;
	}

	public Set<FunctionalProcess> getFunctionalProcesses() {
		return functionalProcesses;
	}
	
	public void addFunctionalProcesses(FunctionalProcess functionalProcess) {
		this.functionalProcesses.add(functionalProcess);
	}
	
	public int functionalPoints() {
		return functionalProcesses.stream().mapToInt(FunctionalProcess::functionalPoints).sum();
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
}
