package org.cfp4j.entity;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class DataGroup extends NamedEntity{

	private final Set<Movement> movements;

	public DataGroup(String name) {
		this(name, new HashSet<>());
	}

	public DataGroup(String name, Movement movement) {
		this(name, Collections.singleton(movement));
	}

	public DataGroup(String name, Set<Movement> movements) {
		super(name);
		this.movements = movements;
	}

	public Set<Movement> getMovements() {
		return movements;
	}
	public void addMovement(Movement movement) {
		movements.add(movement);
	}

	public int functionalPoints() {
		return movements.size();
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
}
