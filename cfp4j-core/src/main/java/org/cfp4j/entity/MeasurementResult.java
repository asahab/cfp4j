package org.cfp4j.entity;

import java.util.Collections;
import java.util.Set;

public class MeasurementResult {
	private final String project;
	private final long startTime;
	private final long duration;
	private final Set<Reference> references;

	public MeasurementResult(String project, long startTime, long duration, Set<Reference> references) {
		super();
		this.project = project;
		this.startTime = startTime;
		this.duration = duration;
		this.references = Collections.unmodifiableSet(references);
	}

	public String getProject() {
		return project;
	}

	public long getStartTime() {
		return startTime;
	}

	public long getDuration() {
		return duration;
	}

	public Set<Reference> getReferences() {
		return references;
	}

	public int getFunctionalPoints() {
		return references.stream().mapToInt(Reference::functionalPoints).sum();
	}
}
