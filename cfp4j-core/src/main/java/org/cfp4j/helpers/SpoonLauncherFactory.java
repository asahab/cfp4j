package org.cfp4j.helpers;


import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spoon.Launcher;
import spoon.MavenLauncher;
import spoon.MavenLauncher.SOURCE_TYPE;

/**
 * Factory class responsible to create a Spoon launcher instance.
 * 
 */
public class SpoonLauncherFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpoonLauncherFactory.class);
	
	/**
	 * Factory method that create a Spoon launcher instance based on the provided source path.
	 * 
	 * If the provided {@code sourcePath} contains a Maven pom file so the instance is {@code MavenLauncher},
	 * otherwise the instance is {@code Launcher}
	 * 
	 * @param sourcePath source code path
	 * @return Launcher instance
	 * @throws NullPointerException if sourcePath is null
	 */
	public Launcher getLauncher(Path sourcePath) {
		final String path = Objects.requireNonNull(sourcePath).toString();
		LOGGER.info("Launcher Instance for the path:{}", path);
		boolean isMavenProject = Paths.get(path, "pom.xml").toFile().exists();
		LOGGER.info("Project Launcher Instance: isMavenProject={}", isMavenProject);
		if (isMavenProject) {
			return new MavenLauncher(path, SOURCE_TYPE.APP_SOURCE);
		}
		Launcher launcher = new Launcher();
		launcher.addInputResource(path);
		return launcher;
	}
}
