package org.cfp4j.helpers;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

import spoon.Launcher;
import spoon.reflect.CtModel;

/**
 * Builder class responsible to create a Spoon compile time module instance.
 * 
 */
public class SpoonCompileTimeModelBuilder {

	private final String projectPath;
	private final SpoonLauncherFactory launcherFactory;

	
	/**
	 * Create SpoonCompileTimeModelBuilder with the provided project path
	 * 
	 * @param projectPath project path
	 * @return SpoonCompileTimeModelBuilder
	 * @throws NullPointerException if projectPath is null
	 */
	public static SpoonCompileTimeModelBuilder projectPath(String projectPath) {
		return new SpoonCompileTimeModelBuilder(projectPath);
	}

	/**
	 * Build {@code CtModel} instance from the provided projectPath and sourceDirectory.
	 * 
	 * @return CtModel instance
	 * @throws  InvalidPathException - if the projectPath string cannot be converted to a Path
	 */
	public CtModel build() {
		Path sourcePath = Paths.get(this.projectPath);
		if (!sourcePath.toFile().isDirectory()) {
			throw new IllegalArgumentException(String.format("Invalid source code Path: Path '%s' is not a directory.", projectPath));
		}
		
		final Launcher launcher = this.launcherFactory.getLauncher(sourcePath);
		launcher.buildModel();
		return launcher.getModel();
	}
	
	private SpoonCompileTimeModelBuilder(String projectPath) {
		this.projectPath = Objects.requireNonNull(projectPath);
		this.launcherFactory = new SpoonLauncherFactory();
	}
}
