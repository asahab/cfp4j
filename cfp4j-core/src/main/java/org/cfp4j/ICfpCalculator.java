package org.cfp4j;

import org.cfp4j.entity.Reference;
import org.cfp4j.helpers.SpoonCompileTimeModelBuilder;

import spoon.reflect.CtModel;

import java.util.Set;

@FunctionalInterface
public interface ICfpCalculator {

	Set<Reference> doCalculation(CtModel model);
	
    default Set<Reference> calculate(String sourceCodePath) {
        final CtModel model = SpoonCompileTimeModelBuilder.projectPath(sourceCodePath).build();
        return doCalculation(model);
    }
}
