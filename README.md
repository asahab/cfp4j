# About CFP4J

Thank you for your interest in COSMIC Functional Points For Java (CFP4J), the automated COSMIC functional size measurement library for Java Web applications.

# Building CFP4J

## What you'll need

- [JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html) or later;
- [Maven 3.1.1](https://maven.apache.org/download.cgi) or later. CFP4J uses Maven as its build tool;
- Your favorite text editor or IDE;
- Connection to the internet required during the build process where Maven download its dependencies.

## Installing Maven

If you don’t have Maven installed yet, visit the following [link](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html).

To test the Maven installation, run MVN from the command-line:

```
mvn -v
```

If the test succeeded, you should get some information similar to the following:

```
Apache Maven 3.6.2 (40f52333136460af0dc0d7232c0dc0bcf0d9e117; 2019-08-27T11:06:16-04:00)
Maven home: C:\..........\apache-maven-3.6.2\bin\..
Java version: 1.8.0_231, vendor: Oracle Corporation, runtime: C:\.....\jre1.8.0_231
Default locale: en_CA, platform encoding: Cp1252
OS name: "windows 10", version: "10.0", arch: "amd64", family: "windows"
```

## Get sources

Clone the source repository using Git: git clone https://gitlab.com/asahab/cfp4j.git

## Structure

1. **CFP4J-Core**: contains the abstract source code common for the different project modules;
1. **CFP4J-SpringMVC**: contains the implementation of the COSMIC fonction point measurment for for Java business web applications using the SpringMVC framework. For more details, visit the following [link](https://gitlab.com/asahab/cfp4j/-/tree/master/cfp4j-springmvc);
1. **CFP4J-Application**: contains the CFP4J library integration sample.

## Build a distribution

To build a distribution of the CFP4J project, run the commands below from the command-line (replace {project_path} with the root path of the project):

```
cd {project_path}
mvn clean package -P distribution
```

If the distribution build succeeded, you should get at the end the following:

```
[INFO] -------------------------------------------------------
[INFO] Reactor Summary for cfp4j 0.0.1-SNAPSHOT:
[INFO] 
[INFO] cfp4j ........................... SUCCESS [  0.206 s]
[INFO] CFP4j Core Module ............... SUCCESS [ 12.648 s]
[INFO] cfp4j-springmvc ................. SUCCESS [  4.230 s]
[INFO] cfp4j-application ............... SUCCESS [  6.604 s]
[INFO] -------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] --------------------------------------------------------
```

After the build, the distribution packages should be located in the path "CFP4J-Application/target/", like below:

```
└── CFP4J-Application
    └── target
        └── cfp4j-application-dist-0.0.1-SNAPSHOT.tar.gz
        └── cfp4j-application-dist-0.0.1-SNAPSHOT.zip
```

The file "cfp4j-application-dist-0.0.1-SNAPSHOT.tar.gz" should be used for a Lunix-based machine.

The file "cfp4j-application-dist-0.0.1-SNAPSHOT.zip" should be used for a Windows-based machine.

Based on your OS, decompress the related file, and you should have the following subdirectory structure and files:

```
└── cfp4j-application-dist-0.0.1-SNAPSHOT
    └── bin
        └── run.bat
        └── run.sh
    └── conf
        └── application.properties
    └── report
    └── cfp4j-application.jar
```

1. **bin**: contains two files related to the chosen OS. The bat file targets the Windows machine and the sh file targets IOS and Lunix machines;
1. **conf**: contains the application configuration file including the following properties of the targeted project for the automated COSMIC functional size measurement:
   
   - **project.name**: corresponds to the project name;
   - **source.code.path**: corresponds to the source code path;
   - **source.code.main.package**: corresponds to the default Java package of the source code;
   - **report.formats**: corresponds to the generated file extensions.
   
1. **report**: empty folder that will contains the generated files containing the result of the automated COSMIC functional size measurement;
1. **cfp4j-application.jar**: represents CFP4J project code package.


#  Test the distribution

To test the final distribution, we used the application "Spring PetClinic Sample". This application integrates Spring MVC and Spring Data. To have more details visit the following [link](https://projects.spring.io/spring-petclinic/).

Below, the steps used:

1. Download and unzip the zip file of the source code from the [link](https://github.com/spring-projects/spring-petclinic/archive/master.zip);
2. Update the "application.properties" file as the following (If you use a Windows machine, you should use "\\\\" as folder separator for the value of the property "source.code.path"):
   
```
project.name=Spring Pet Clinic
source.code.path=C:\\path...\\spring-petclinic-master
source.code.main.package=org.springframework.samples.petclinic
report.formats=excel,json
```

3. Based on your OS machine, execute the run file located in the folder "bin";
4. If the test succeeded, you should get the following at the end:

```
=========================================================================

COSMIC CFP4J APP Environment

APP_HOME: "C:\..............\cfp4j-application-dist-0.0.1-SNAPSHOT"

JAVA_HOME: ""

JAVA_OPTS: "-Xms1024m -Xmx2048m -Xmn1360m -Dapp.report.location=C:\...\cfp4j-application-dist-0.0.1-SNAPSHOT\report -Dapp.config.location=C:\...\cfp4j-application-dist-0.0.1-SNAPSHOT\conf\application.properties"

=========================================================================

[main] INFO org.cfp4j.helpers.SpoonLauncherFactory - Launcher Instance for the path:C:\Users\usename\...............\cfp4j-projects-for-test\spring-petclinic-master
[main] INFO org.cfp4j.helpers.SpoonLauncherFactory - Project Launcher Instance: isMavenProject=true
[main] INFO org.cfp4j.springmvc.SpringMvcCfpCalculator - Starting the CFP calculation: controller_count=6
Press any key to continue . . .
```

5. Press enter;
6. Check that the folder "report" contains 2 new files as below:
   
```
!__ cfp4j-application-dist-0.0.1-SNAPSHOT
    !__ ....
    !__ report
        !__ cosmic-metric-Spring_Pet_Clinic-XXXXXXXXXXXXX.xlsx
        !__ cosmic-metric-Spring_Pet_Clinic-XXXXXXXXXXXXX.json
```

#  Contributing

You can help to improve the the CFP4J project by using the [issue tracker](https://gitlab.com/asahab/cfp4j/issues) that is the main channel for bug reports, features requests, and submitting pull requests.

To make a welcoming environment for everyone who is interested in contributing, the editor preferences are available in the [editor config](https://gitlab.com/asahab/cfp4j/blob/master/.editorconfig) for easy use in common text editors. Read more and download plugins at [editorconfig.org](https://editorconfig.org). 

Feel free to make a proposal, as we are open for every discussion. Thank you for your interest and help!

#  License

The CFP4J project is released under version 2.0 of the [Apache License](https://www.apache.org/licenses/LICENSE-2.0).


